(ql:quickload :alexandria)

(defun factorial (n)
  (let ((result 1))
    (dotimes (i n result)
      (setf result (* result (1+ i))))))

(defun over (n k)
  (/ (factorial n) (factorial (- n k)) (factorial k)))

(defun get-type (dimension subdim)
  (format nil "MV~a~{_~a~}" dimension subdim))

(defun get-len (dimension subdim)
  (reduce (lambda (old new) (+ old (over dimension new))) subdim :initial-value 0))

(defun range (n)
  (loop for i below n collect i))

(defun single-true (i n)
  (let ((list (make-list n)))
    (setf (nth i list) t)
    list))

(defun range-single-true (n)
  (loop for i below n collect (single-true i n)))

(defun range-vars (n)
  (loop for i below n collect (format nil "a~a" i)))

(defun list-range (n)
  (loop for i below n collect (list i)))

(defun range-range (n)
  (loop for i below n collect i collect i))

(defun space-range (n)
  (loop for i below n collect #\ ))

(defun ilist (i n)
  (let ((rest (if (< i n) (ilist (1+ i) n))))
    (let (result)
      (dolist (list (cons nil rest))
        (push (copy-list list) result)
        (push (cons i list) result))
      result)))

(defun sort-len-number (list list0)
  (let ((len (length list)) (len0 (length list0)))
    (if (< len len0)
        t
        (if (< len0 len)
            nil
            (loop for num in list
               for num0 in list0
               do (if (< num num0)
                      (return t)
                      (if (< num0 num)
                          (return nil))))))))

(defun all-subsets (n)
  (sort (reverse (remove-duplicates (mapcar (lambda (list) (sort list #'<)) (ilist 0 n)) :test #'equal))
        #'sort-len-number))

(defun get-all-impl (stream max)
  (format stream
"use {Inner, Outer};

use vector_space::{VectorSpace,InnerSpace};

use num_traits::{One,Zero,Num};
use num_traits::real::Real;

use std::ops::{
    Add,
    Sub,
    Mul,
    Div,
    Neg,
    Rem,
    AddAssign,
    SubAssign,
    MulAssign,
    DivAssign
};

use std::cmp::PartialEq;
")
  (dotimes (dim (1+ max))
    (dolist (subdim (all-subsets dim))
      (get-impl stream dim subdim))))

(defun get-impl (stream dimension subdim)
  (let ((subdim (remove-duplicates (sort subdim #'<))))
    (let ((len (get-len dimension subdim))
          (type (get-type dimension subdim)))
      (format stream "
#[derive(Copy,Clone,Debug)]
pub struct ~a<T> (
    [T;~a]
);
" type len)
      (when (and subdim (null (cdr subdim)))
        (impl-constructor stream type len))
      (when (and subdim (zerop (car subdim)) (null (cdr subdim)))
        (impl-scalar stream type)
        (impl-unit stream type))
      (when (and subdim (< 0 dimension) (= dimension (car subdim)) (null (cdr subdim)))
        (impl-unit stream type))
      (when (and subdim (= 1 (car subdim)) (null (cdr subdim)))
        (impl-e stream type len))
      (impl-vector-space stream type)
      (impl-inner-space stream type)
      (impl-math stream type)
      (impl-reverse stream dimension type subdim)
      (impl-exp stream type dimension subdim)
      (add-trait stream type len)
      (sub-trait stream type len)
      (neg-trait stream type len)
      (mul-trait-simple stream type len)
      (div-trait-simple stream type len)
      (rem-trait-simple stream type len)
      (add-assign-trait stream type len)
      (sub-assign-trait stream type len)
      (mul-assign-trait-simple stream type len)
      (div-assign-trait-simple stream type len)
      (zero-trait stream type len)
      (peq-trait stream type len)
      (when (and subdim (zerop (car subdim)) (equal (get-mul-dimension dimension subdim subdim) subdim))
        (one-trait stream type len))
      (when (and subdim (zerop (car subdim)))
        (simple-from-trait stream type len))
      (dolist (subdim0 (all-subsets dimension))
        (unless (equal subdim subdim0)
          (from-trait stream dimension type (get-type dimension subdim0) subdim subdim0))
        (mul-trait stream dimension type (get-type dimension subdim0) subdim subdim0)
        (inner-trait stream dimension type (get-type dimension subdim0) subdim subdim0)
        (outer-trait stream dimension type (get-type dimension subdim0) subdim subdim0)
      ))))


(defun impl-constructor (stream type len)
  (format stream "
impl<T> ~a<T> {
    pub fn new(~{a~a: T~^,~}) -> Self {
        ~a (
            [~{a~a~^,
             ~}]
        )
    }
}
" type (range len) type (range len)))

(defun impl-unit (stream type)
  (format stream "
impl<T> ~a<T> where T: One {
    pub fn unit() -> Self {
        ~a (
            [T::one()]
        )
    }
}
" type type))

(defun impl-scalar (stream type)
  (format stream "
impl<T> ~a<T> where T: One {
    pub fn value(self) -> T where T: Copy {
        self.0[0]
    }
}
" type))

(defun impl-e (stream type len)
  (format stream "
impl<T> ~a<T> where T: Zero+One {
    pub fn vector(self) -> [T;~a] {
        self.0
    }
~{    pub fn e~a() -> Self {
        ~a (
            [~{~:[T::zero()~;T::one()~]~^,~}]
        )
    }
~}}
" type len (alexandria:mappend (lambda (i) (list (1+ i) type (single-true i len))) (range len))))

(defun impl-vector-space (stream type)
  (format stream "
impl<T> VectorSpace for ~a<T> where T: Num+Copy {
    type Scalar = T;
}
" type))

(defun impl-inner-space (stream type)
  (format stream "
impl<T> InnerSpace for ~a<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}
" type))

(defun impl-math (stream type)
  (format stream "
/*impl<T> ~a<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> ~a<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}
" type type))

(defun smallest-superalgebra (dimension subdim)
  (let ((multiple (apply #'gcd subdim)))
    (if (not (zerop multiple))
        (loop for mul from 0 to dimension by multiple
           collect mul)
        subdim)))

(defun impl-exp (stream type dimension subdim)
  (let* ((out-dim (smallest-superalgebra dimension subdim))
         ;(out-dim (if (every #'evenp sup-dim) sup-dim (alexandria:iota dimension)))
         (out-type (get-type dimension out-dim)))
    (if out-dim
        (format stream "
impl<T> ~a<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> ~a<T> {
        let mut z = ~a::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = ~a::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}
" type out-type out-type out-type)
        (format stream "impl<T> ~a<T> where T: Real+AddAssign {
    pub fn exp(self) -> Self {
        ~a([])
    }
}" type type))))

  (defun add-trait (stream type len)
  (format stream "
impl<T> Add for ~a<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  ~:[other~;_~]: Self) -> Self {
        ~:[let a = self.0;
        let b = other.0;
        ~;~]~a (
            [~{a[~a] + b[~a]~^,
             ~}]
        )
    }
}
" type (zerop len) (zerop len) type (range-range len)))

(defun sub-trait (stream type len)
  (format stream "
impl<T> Sub for ~a<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  ~:[other~;_~]: Self) -> Self {
        ~:[let a = self.0;
        let b = other.0;
        ~;~]~a (
            [~{a[~a] - b[~a]~^,
             ~}]
        )
    }
}
" type (zerop len) (zerop len) type (range-range len)))

(defun neg-trait (stream type len)
  (format stream "
impl<T> Neg for ~a<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        ~:[let a = self.0;
        ~;~]~a (
            [~{-a[~a]~^,
             ~}]
        )
    }
}
" type (zerop len) type (range len)))

(defun general-from-trait (stream type len)
  (format stream "
impl<T,U> From<U> for ~a<T> where T: From<U> {
    fn from(other: U) -> Self {
        ~:[let a = other.0;
        ~;~]~a (
            [~{a[~a].into()~^,
             ~}]
        )
    }
}
" type (zerop len) type (range len)))

(defun mul-trait-simple (stream type len)
  (format stream "
impl<T> Mul<T> for ~a<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  ~:[other~;_~]: T) -> Self {
        ~:[let a = self.0;
        ~;~]~a (
            [~{a[~a] * other~^,
             ~}]
        )
    }
}
" type (zerop len) (zerop len) type (range len)))

(defun div-trait-simple (stream type len)
  (format stream "
impl<T> Div<T> for ~a<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  ~:[other~;_~]: T) -> Self {
        ~:[let a = self.0;
        ~;~]~a (
            [~{a[~a] / other~^,
             ~}]
        )
    }
}
" type (zerop len) (zerop len) type (range len)))

(defun rem-trait-simple (stream type len)
  (format stream "
impl<T> Rem<T> for ~a<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  ~:[other~;_~]: T) -> Self {
        ~:[let a = self.0;
        ~;~]~a (
            [~{a[~a] % other~^,
             ~}]
        )
    }
}
" type (zerop len) (zerop len) type (range len)))

(defun add-assign-trait (stream type len)
  (format stream "
impl<T> AddAssign for ~a<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, ~:[other~;_~]: Self) {
        ~:[let ref mut a = self.0;
        let b = other.0;
        ~;~]~{a[~a] += b[~a];~^
        ~}
    }
}
" type (zerop len) (zerop len) (range-range len)))

(defun sub-assign-trait (stream type len)
  (format stream "
impl<T> SubAssign for ~a<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, ~:[other~;_~]: Self) {
        ~:[let ref mut a = self.0;
        let b = other.0;
        ~;~]~{a[~a] -= b[~a];~^
        ~}
    }
}
" type (zerop len) (zerop len) (range-range len)))


(defun mul-assign-trait-simple (stream type len)
  (format stream "
impl<T> MulAssign<T> for ~a<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, ~:[other~;_~]: T) {
        ~:[let ref mut a = self.0;
        ~;~]~{a[~a] *= other;~^
        ~}
    }
}
" type (zerop len) (zerop len) (range len)))

(defun div-assign-trait-simple (stream type len)
  (format stream "
impl<T> DivAssign<T> for ~a<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, ~:[other~;_~]: T) {
        ~:[let ref mut a = self.0;
        ~;~]~{a[~a] /= other;~^
        ~}
    }
}
" type (zerop len) (zerop len) (range len)))

(defun peq-trait (stream type len)
  (format stream "
impl<T> PartialEq for ~a<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        ~{self.0[~a]==other.0[~a]&&
        ~}true
    }
}
" type (range-range len)))

(defun zero-trait (stream type len)
  (format stream "
impl<T> Zero for ~a<T> where T: Zero+Copy {
    fn zero() -> Self { ~a ([~{~aT::zero()~^,~}]) }
    fn is_zero(&self) -> bool {
        ~{self.0[~a].is_zero()&&
        ~}true
    }
}
" type type (space-range len) (range len)))

(defun one-trait (stream type len)
  (format stream "
impl<T> One for ~a<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { ~a ([~{~:[T::zero()~;T::one()~]~^,~}]) }
}
" type type (single-true 0 len)))


(defun simple-from-trait (stream type len)
  (format stream "
impl<T> From<T> for ~a<T>~:[ where T: Zero~;~] {
    fn from(a: T) -> Self {
        ~a (
            [a~{,~:[~;T::zero()~]~}]
        )
    }
}
" type (zerop len) type (range (1- len))))


(defun get-mul-dimension-old (dimension subdim0 subdim1)
  (let (result)
    (dolist (num0 subdim0)
      (dolist (num1 subdim1)
        (push (abs (- num1 num0)) result)
        (push (+ num1 num0) result)))
    (sort (remove-duplicates (remove-if (lambda (x) (< dimension x)) result)) #'<)))

(defun get-mul-dimension (dimension subdim0 subdim1)
  (let (result)
    (dolist (a (subdim-single dimension subdim0))
      (dolist (b (subdim-single dimension subdim1))
        (multiple-value-bind (val x u) (single-mul a b)
          (declare (ignore x))
          ;;(format t "~a ~a = ~a~%" a b val)
          (unless u (push val result)))))
    (sort (remove-duplicates (mapcar #'length result)) #'<)))

(defun get-inner-dimension-old (dimension subdim0 subdim1)
  (let (result)
    (dolist (num0 subdim0)
      (dolist (num1 subdim1)
        (push (abs (- num1 num0)) result)))
    (sort (remove-duplicates (remove-if (lambda (x) (< dimension x)) result)) #'<)))

(defun get-inner-dimension (dimension subdim0 subdim1)
  (let (result)
    (dolist (a (subdim-single dimension subdim0))
      (dolist (b (subdim-single dimension subdim1))
        (multiple-value-bind (val x u) (single-inner a b)
          (declare (ignore x))
          (unless u (push val result)))))
    (sort (remove-duplicates (mapcar #'length result)) #'<)))

(defun get-outer-dimension (dimension subdim0 subdim1)
  (let (result)
    (dolist (num0 subdim0)
      (dolist (num1 subdim1)
        (push (+ num1 num0) result)))
    (sort (remove-duplicates (remove-if (lambda (x) (< dimension x)) result)) #'<)))

(defun subdim-single (dimension subdim)
  (remove-if-not (lambda (element) (member (length element) subdim)) (all-subsets (1- dimension))))

(defun remove-facs (list)
  (let (ret fac)
    (loop while list
       do (let* ((first (car list))
                 (pos (position first (cdr list)))
                 (new-fac (if (or (not pos) (oddp pos)) (not fac) fac))
                 (new-list (if pos (remove first (cdr list) :count 1) (cdr list))))
            (unless pos
              (push first ret))
            (setf list new-list fac new-fac)))
    (setf ret (reverse ret))
    (let ((sorted (sort (copy-list ret) #'<)))
      (let ((s2 (remove-duplicates sorted)))
        (unless (= (length sorted) (length s2))
          (format t "sorted: ~a (not ~a)~%" sorted ret)))
      (dolist (val sorted)
        (let ((pos (position val ret)))
          (when (evenp pos)
            (setf fac (not fac)))
          (setf ret (delete val ret :count 1))))
      (values sorted fac))))

(defun single-mul (a b)
  (let ((all (append a b)))
    (remove-facs all)))

(defun single-inner (a b)
  (let ((ab (append a b))
        (ba (append b a)))
    (multiple-value-bind (abr abf) (remove-facs ab)
      (multiple-value-bind (bar baf) (remove-facs ba)
        ;(format t "~a*~a = ~:[~;-~]~a~%" a b abf abr)
        (assert (equal abr bar) () "Remove facs for single-inner should be equal")
        (if (= (length abr) (- (max (length a) (length b)) (min (length a) (length b))))
            (values abr abf)
            (values nil nil t))))))

(defun single-outer (a b)
  (let ((all (append a b)))
    (let ((test (remove-duplicates all)))
      (if (< (length test) (length all))
          (values nil nil t)
          (remove-facs all)))))

(defun get-field (dimension subdim single)
  (position single (subdim-single dimension subdim) :test #'equal))

(defun get-mul-array (dimension subdim subdim0 out-dim)
  (let ((format-list (list-range (get-len dimension out-dim))))
    (let ((single (subdim-single dimension subdim))
          (single0 (subdim-single dimension subdim0)))
      (dolist (single single)
        (dolist (single0 single0)
          (multiple-value-bind (new-single fac) (single-mul single single0)
            (let* ((out-pos (get-field dimension out-dim new-single))
                   (push-field (assoc out-pos format-list)))
              (push (get-field dimension subdim0 single0) (cdr push-field))
              (push (get-field dimension subdim single) (cdr push-field))
              (push (if fac 1 0) (cdr push-field)))))))
    (format nil "
            [~{~{~[+~;-~; ~] a[~a]*b[~a]~^ ~}~^,
             ~}]
" (mapcar (lambda (list) (if (and list (= (car list) 0)) (cons 2 (cdr list)) list)) (mapcar #'cdr format-list)))))

(defun get-from-array (dimension subdim out-dim)
  (let ((format-list (list-range (get-len dimension out-dim))))
    (let ((single (subdim-single dimension (intersection subdim out-dim))))
      (dolist (single single)
        (let* ((out-pos (get-field dimension out-dim single))
               (push-field (assoc out-pos format-list)))
          (setf (cdr push-field) (get-field dimension subdim single)))))
    ;(format t "~a~%" format-list) 
    (format nil "
            [~{~a~^,
             ~}]
" (mapcar (lambda (val) (if val (format nil "a[~a]" val) "T::zero()")) (mapcar #'cdr format-list)))))

(defun get-outer-array (dimension subdim subdim0 out-dim)
  (let ((format-list (list-range (get-len dimension out-dim))))
    (let ((single (subdim-single dimension subdim))
          (single0 (subdim-single dimension subdim0)))
      (dolist (single single)
        (dolist (single0 single0)
          (multiple-value-bind (new-single fac call) (single-outer single single0)
            (unless call
              (let* ((out-pos (get-field dimension out-dim new-single))
                     (push-field (assoc out-pos format-list)))
                (push (get-field dimension subdim0 single0) (cdr push-field))
                (push (get-field dimension subdim single) (cdr push-field))
                (push (if fac 1 0) (cdr push-field))))))))
    (format nil "
            [~{~{~[+~;-~; ~] a[~a]*b[~a]~^ ~}~^,
             ~}]
" (mapcar (lambda (list) (if (and list (= (car list) 0)) (cons 2 (cdr list)) list)) (mapcar #'cdr format-list)))))

(defun get-inner-array (dimension subdim subdim0 out-dim)
  (let ((format-list (list-range (get-len dimension out-dim))))
    (let ((single (subdim-single dimension subdim))
          (single0 (subdim-single dimension subdim0)))
      (dolist (single single)
        (dolist (single0 single0)
          (multiple-value-bind (new-single fac call) (single-inner single single0)
            (unless call
              (let* ((out-pos (get-field dimension out-dim new-single))
                     (push-field (assoc out-pos format-list)))
                (push (get-field dimension subdim0 single0) (cdr push-field))
                (push (get-field dimension subdim single) (cdr push-field))
                (push (if fac 1 0) (cdr push-field))))))))
    (format nil "
            [~{~:[T::zero()~:[~;~]~;~{~[+~;-~; ~] a[~a]*b[~a]~^ ~}~]~^,
             ~}]
" (alexandria:mappend (lambda (list) (let ((list (if (and list (= (car list) 0)) (cons 2 (cdr list)) list)))
                           (list list list)))
          (mapcar #'cdr format-list)))))

    
(defun mul-trait (stream dimension type type0 subdim subdim0)
  (let* ((out-dim (get-mul-dimension dimension subdim subdim0))
         (out-type (get-type dimension out-dim)))
    (format stream "
impl<T> Mul<~a<T>> for ~a<T> where T: Num+Neg<Output=T>+Copy {
    type Output = ~a<T>;
    fn mul(self,  ~:[other~;_~]: ~a<T>) -> ~a<T> {
        ~:[let a = self.0;
        let b = other.0;
        ~;~]~a(~a)
    }
}
" type0 type out-type (zerop (get-len dimension out-dim)) type0 out-type (zerop (get-len dimension out-dim)) out-type (get-mul-array dimension subdim subdim0 out-dim))))

(defun inner-trait (stream dimension type type0 subdim subdim0)
  (let* ((out-dim (get-inner-dimension-old dimension subdim subdim0))
         (out-type (get-type dimension out-dim)))
    (format stream "
impl<T> Inner<~a<T>> for ~a<T> where T: Num+Neg<Output=T>+Copy {
    type Output = ~a<T>;
    fn inner(self, ~:[other~;_~]: ~a<T>) -> ~a<T> {
        ~:[let a = self.0;
        let b = other.0;
        ~;~]~a(~a)
    }
}
" type0 type out-type (zerop (get-len dimension out-dim)) type0 out-type (zerop (get-len dimension out-dim)) out-type (get-inner-array dimension subdim subdim0 out-dim))))

(defun outer-trait (stream dimension type type0 subdim subdim0)
  (let* ((out-dim (get-outer-dimension dimension subdim subdim0))
         (out-type (get-type dimension out-dim)))
    (format stream "
impl<T> Outer<~a<T>> for ~a<T> where T: Num+Neg<Output=T>+Copy {
    type Output = ~a<T>;
    fn outer(self, ~:[other~;_~]: ~a<T>) -> ~a<T> {
        ~:[let a = self.0;
        let b = other.0;
        ~;~]~a(~a)
    }
}
" type0 type out-type (zerop (get-len dimension out-dim)) type0 out-type (zerop (get-len dimension out-dim)) out-type (get-outer-array dimension subdim subdim0 out-dim))))

(defun from-trait (stream dimension type out-type subdim out-dim)
  (format stream "
impl<T> From<~a<T>> for ~a<T> where T: Zero+Copy {
    fn from(a: ~a<T>) -> Self {
        ~:[let a = a.0;
        ~;~]~a(~a)
    }
}
" type out-type type (null out-dim) out-type (get-from-array dimension subdim out-dim)))

(defun impl-reverse (stream dimension type subdim)
  (format stream "
impl<T> ~a<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        ~a(
            [~{~:[-~; ~]self.0[~a]~^,
             ~}]
        )
    }
}
" type type (alexandria:mappend (lambda (single i) (list (evenp (floor (length single) 2)) i)) (subdim-single dimension subdim) (range (get-len dimension subdim)))))


(with-open-file (stream "src/auto.rs" :direction :output :if-exists :supersede :if-does-not-exist :create)
  (get-all-impl stream 3))

use {Inner, Outer};

use vector_space::{VectorSpace,InnerSpace};

use num_traits::{One,Zero,Num};
use num_traits::real::Real;

use std::ops::{
    Add,
    Sub,
    Mul,
    Div,
    Neg,
    Rem,
    AddAssign,
    SubAssign,
    MulAssign,
    DivAssign
};

use std::cmp::PartialEq;

#[derive(Copy,Clone,Debug)]
pub struct MV0<T> (
    [T;0]
);

impl<T> VectorSpace for MV0<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV0<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV0<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV0(
            []
        )
    }
}
impl<T> MV0<T> where T: Real+AddAssign {
    pub fn exp(self) -> Self {
        MV0([])
    }
}
impl<T> Add for MV0<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  _: Self) -> Self {
        MV0 (
            []
        )
    }
}

impl<T> Sub for MV0<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  _: Self) -> Self {
        MV0 (
            []
        )
    }
}

impl<T> Neg for MV0<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        MV0 (
            []
        )
    }
}

impl<T> Mul<T> for MV0<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  _: T) -> Self {
        MV0 (
            []
        )
    }
}

impl<T> Div<T> for MV0<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  _: T) -> Self {
        MV0 (
            []
        )
    }
}

impl<T> Rem<T> for MV0<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  _: T) -> Self {
        MV0 (
            []
        )
    }
}

impl<T> AddAssign for MV0<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, _: Self) {
        
    }
}

impl<T> SubAssign for MV0<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, _: Self) {
        
    }
}

impl<T> MulAssign<T> for MV0<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, _: T) {
        
    }
}

impl<T> DivAssign<T> for MV0<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, _: T) {
        
    }
}

impl<T> Zero for MV0<T> where T: Zero+Copy {
    fn zero() -> Self { MV0 ([]) }
    fn is_zero(&self) -> bool {
        true
    }
}

impl<T> PartialEq for MV0<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        true
    }
}

impl<T> Mul<MV0<T>> for MV0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn mul(self,  _: MV0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

impl<T> Inner<MV0<T>> for MV0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn inner(self, _: MV0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

impl<T> Outer<MV0<T>> for MV0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn outer(self, _: MV0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

impl<T> From<MV0<T>> for MV0_0<T> where T: Zero+Copy {
    fn from(a: MV0<T>) -> Self {
        let a = a.0;
        MV0_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV0_0<T>> for MV0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn mul(self,  _: MV0_0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

impl<T> Inner<MV0_0<T>> for MV0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn inner(self, _: MV0_0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

impl<T> Outer<MV0_0<T>> for MV0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn outer(self, _: MV0_0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV0_0<T> (
    [T;1]
);

impl<T> MV0_0<T> {
    pub fn new(a0: T) -> Self {
        MV0_0 (
            [a0]
        )
    }
}

impl<T> MV0_0<T> where T: One {
    pub fn value(self) -> T where T: Copy {
        self.0[0]
    }
}

impl<T> MV0_0<T> where T: One {
    pub fn unit() -> Self {
        MV0_0 (
            [T::one()]
        )
    }
}

impl<T> VectorSpace for MV0_0<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV0_0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV0_0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV0_0<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV0_0<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV0_0(
            [ self.0[0]]
        )
    }
}

impl<T> MV0_0<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV0_0<T> {
        let mut z = MV0_0::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV0_0::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV0_0<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV0_0 (
            [a[0] + b[0]]
        )
    }
}

impl<T> Sub for MV0_0<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV0_0 (
            [a[0] - b[0]]
        )
    }
}

impl<T> Neg for MV0_0<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV0_0 (
            [-a[0]]
        )
    }
}

impl<T> Mul<T> for MV0_0<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV0_0 (
            [a[0] * other]
        )
    }
}

impl<T> Div<T> for MV0_0<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV0_0 (
            [a[0] / other]
        )
    }
}

impl<T> Rem<T> for MV0_0<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV0_0 (
            [a[0] % other]
        )
    }
}

impl<T> AddAssign for MV0_0<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
    }
}

impl<T> SubAssign for MV0_0<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
    }
}

impl<T> MulAssign<T> for MV0_0<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
    }
}

impl<T> DivAssign<T> for MV0_0<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
    }
}

impl<T> Zero for MV0_0<T> where T: Zero+Copy {
    fn zero() -> Self { MV0_0 ([ T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV0_0<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        true
    }
}

impl<T> One for MV0_0<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV0_0 ([T::one()]) }
}

impl<T> From<T> for MV0_0<T> where T: Zero {
    fn from(a: T) -> Self {
        MV0_0 (
            [a]
        )
    }
}

impl<T> From<MV0_0<T>> for MV0<T> where T: Zero+Copy {
    fn from(a: MV0_0<T>) -> Self {
        MV0(
            []
)
    }
}

impl<T> Mul<MV0<T>> for MV0_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn mul(self,  _: MV0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

impl<T> Inner<MV0<T>> for MV0_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn inner(self, _: MV0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

impl<T> Outer<MV0<T>> for MV0_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0<T>;
    fn outer(self, _: MV0<T>) -> MV0<T> {
        MV0(
            []
)
    }
}

impl<T> Mul<MV0_0<T>> for MV0_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0_0<T>;
    fn mul(self,  other: MV0_0<T>) -> MV0_0<T> {
        let a = self.0;
        let b = other.0;
        MV0_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV0_0<T>> for MV0_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0_0<T>;
    fn inner(self, other: MV0_0<T>) -> MV0_0<T> {
        let a = self.0;
        let b = other.0;
        MV0_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV0_0<T>> for MV0_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV0_0<T>;
    fn outer(self, other: MV0_0<T>) -> MV0_0<T> {
        let a = self.0;
        let b = other.0;
        MV0_0(
            [  a[0]*b[0]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV1<T> (
    [T;0]
);

impl<T> VectorSpace for MV1<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV1<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV1<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV1(
            []
        )
    }
}
impl<T> MV1<T> where T: Real+AddAssign {
    pub fn exp(self) -> Self {
        MV1([])
    }
}
impl<T> Add for MV1<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  _: Self) -> Self {
        MV1 (
            []
        )
    }
}

impl<T> Sub for MV1<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  _: Self) -> Self {
        MV1 (
            []
        )
    }
}

impl<T> Neg for MV1<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        MV1 (
            []
        )
    }
}

impl<T> Mul<T> for MV1<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  _: T) -> Self {
        MV1 (
            []
        )
    }
}

impl<T> Div<T> for MV1<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  _: T) -> Self {
        MV1 (
            []
        )
    }
}

impl<T> Rem<T> for MV1<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  _: T) -> Self {
        MV1 (
            []
        )
    }
}

impl<T> AddAssign for MV1<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, _: Self) {
        
    }
}

impl<T> SubAssign for MV1<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, _: Self) {
        
    }
}

impl<T> MulAssign<T> for MV1<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, _: T) {
        
    }
}

impl<T> DivAssign<T> for MV1<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, _: T) {
        
    }
}

impl<T> Zero for MV1<T> where T: Zero+Copy {
    fn zero() -> Self { MV1 ([]) }
    fn is_zero(&self) -> bool {
        true
    }
}

impl<T> PartialEq for MV1<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        true
    }
}

impl<T> Mul<MV1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn mul(self,  _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Inner<MV1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn inner(self, _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Outer<MV1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn outer(self, _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> From<MV1<T>> for MV1_0<T> where T: Zero+Copy {
    fn from(a: MV1<T>) -> Self {
        let a = a.0;
        MV1_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV1_0<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn mul(self,  _: MV1_0<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Inner<MV1_0<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn inner(self, _: MV1_0<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Outer<MV1_0<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn outer(self, _: MV1_0<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> From<MV1<T>> for MV1_1<T> where T: Zero+Copy {
    fn from(a: MV1<T>) -> Self {
        let a = a.0;
        MV1_1(
            [T::zero()]
)
    }
}

impl<T> Mul<MV1_1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn mul(self,  _: MV1_1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Inner<MV1_1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn inner(self, _: MV1_1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Outer<MV1_1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn outer(self, _: MV1_1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> From<MV1<T>> for MV1_0_1<T> where T: Zero+Copy {
    fn from(a: MV1<T>) -> Self {
        let a = a.0;
        MV1_0_1(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV1_0_1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn mul(self,  _: MV1_0_1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Inner<MV1_0_1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn inner(self, _: MV1_0_1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Outer<MV1_0_1<T>> for MV1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn outer(self, _: MV1_0_1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV1_0<T> (
    [T;1]
);

impl<T> MV1_0<T> {
    pub fn new(a0: T) -> Self {
        MV1_0 (
            [a0]
        )
    }
}

impl<T> MV1_0<T> where T: One {
    pub fn value(self) -> T where T: Copy {
        self.0[0]
    }
}

impl<T> MV1_0<T> where T: One {
    pub fn unit() -> Self {
        MV1_0 (
            [T::one()]
        )
    }
}

impl<T> VectorSpace for MV1_0<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV1_0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV1_0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV1_0<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV1_0<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV1_0(
            [ self.0[0]]
        )
    }
}

impl<T> MV1_0<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV1_0<T> {
        let mut z = MV1_0::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV1_0::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV1_0<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV1_0 (
            [a[0] + b[0]]
        )
    }
}

impl<T> Sub for MV1_0<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV1_0 (
            [a[0] - b[0]]
        )
    }
}

impl<T> Neg for MV1_0<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV1_0 (
            [-a[0]]
        )
    }
}

impl<T> Mul<T> for MV1_0<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV1_0 (
            [a[0] * other]
        )
    }
}

impl<T> Div<T> for MV1_0<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV1_0 (
            [a[0] / other]
        )
    }
}

impl<T> Rem<T> for MV1_0<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV1_0 (
            [a[0] % other]
        )
    }
}

impl<T> AddAssign for MV1_0<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
    }
}

impl<T> SubAssign for MV1_0<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
    }
}

impl<T> MulAssign<T> for MV1_0<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
    }
}

impl<T> DivAssign<T> for MV1_0<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
    }
}

impl<T> Zero for MV1_0<T> where T: Zero+Copy {
    fn zero() -> Self { MV1_0 ([ T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV1_0<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        true
    }
}

impl<T> One for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV1_0 ([T::one()]) }
}

impl<T> From<T> for MV1_0<T> where T: Zero {
    fn from(a: T) -> Self {
        MV1_0 (
            [a]
        )
    }
}

impl<T> From<MV1_0<T>> for MV1<T> where T: Zero+Copy {
    fn from(a: MV1_0<T>) -> Self {
        MV1(
            []
)
    }
}

impl<T> Mul<MV1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn mul(self,  _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Inner<MV1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn inner(self, _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Outer<MV1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn outer(self, _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Mul<MV1_0<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0<T>;
    fn mul(self,  other: MV1_0<T>) -> MV1_0<T> {
        let a = self.0;
        let b = other.0;
        MV1_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV1_0<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0<T>;
    fn inner(self, other: MV1_0<T>) -> MV1_0<T> {
        let a = self.0;
        let b = other.0;
        MV1_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV1_0<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0<T>;
    fn outer(self, other: MV1_0<T>) -> MV1_0<T> {
        let a = self.0;
        let b = other.0;
        MV1_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV1_0<T>> for MV1_1<T> where T: Zero+Copy {
    fn from(a: MV1_0<T>) -> Self {
        let a = a.0;
        MV1_1(
            [T::zero()]
)
    }
}

impl<T> Mul<MV1_1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_1<T>;
    fn mul(self,  other: MV1_1<T>) -> MV1_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_1(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV1_1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_1<T>;
    fn inner(self, other: MV1_1<T>) -> MV1_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_1(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV1_1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_1<T>;
    fn outer(self, other: MV1_1<T>) -> MV1_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_1(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV1_0<T>> for MV1_0_1<T> where T: Zero+Copy {
    fn from(a: MV1_0<T>) -> Self {
        let a = a.0;
        MV1_0_1(
            [a[0],
             T::zero()]
)
    }
}

impl<T> Mul<MV1_0_1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn mul(self,  other: MV1_0_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV1_0_1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn inner(self, other: MV1_0_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV1_0_1<T>> for MV1_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn outer(self, other: MV1_0_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV1_1<T> (
    [T;1]
);

impl<T> MV1_1<T> {
    pub fn new(a0: T) -> Self {
        MV1_1 (
            [a0]
        )
    }
}

impl<T> MV1_1<T> where T: One {
    pub fn unit() -> Self {
        MV1_1 (
            [T::one()]
        )
    }
}

impl<T> MV1_1<T> where T: Zero+One {
    pub fn vector(self) -> [T;1] {
        self.0
    }
    pub fn e1() -> Self {
        MV1_1 (
            [T::one()]
        )
    }
}

impl<T> VectorSpace for MV1_1<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV1_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV1_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV1_1<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV1_1<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV1_1(
            [ self.0[0]]
        )
    }
}

impl<T> MV1_1<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV1_0_1<T> {
        let mut z = MV1_0_1::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV1_0_1::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV1_1<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV1_1 (
            [a[0] + b[0]]
        )
    }
}

impl<T> Sub for MV1_1<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV1_1 (
            [a[0] - b[0]]
        )
    }
}

impl<T> Neg for MV1_1<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV1_1 (
            [-a[0]]
        )
    }
}

impl<T> Mul<T> for MV1_1<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV1_1 (
            [a[0] * other]
        )
    }
}

impl<T> Div<T> for MV1_1<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV1_1 (
            [a[0] / other]
        )
    }
}

impl<T> Rem<T> for MV1_1<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV1_1 (
            [a[0] % other]
        )
    }
}

impl<T> AddAssign for MV1_1<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
    }
}

impl<T> SubAssign for MV1_1<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
    }
}

impl<T> MulAssign<T> for MV1_1<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
    }
}

impl<T> DivAssign<T> for MV1_1<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
    }
}

impl<T> Zero for MV1_1<T> where T: Zero+Copy {
    fn zero() -> Self { MV1_1 ([ T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV1_1<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        true
    }
}

impl<T> From<MV1_1<T>> for MV1<T> where T: Zero+Copy {
    fn from(a: MV1_1<T>) -> Self {
        MV1(
            []
)
    }
}

impl<T> Mul<MV1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn mul(self,  _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Inner<MV1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn inner(self, _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Outer<MV1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn outer(self, _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> From<MV1_1<T>> for MV1_0<T> where T: Zero+Copy {
    fn from(a: MV1_1<T>) -> Self {
        let a = a.0;
        MV1_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV1_0<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_1<T>;
    fn mul(self,  other: MV1_0<T>) -> MV1_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_1(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV1_0<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_1<T>;
    fn inner(self, other: MV1_0<T>) -> MV1_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_1(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV1_0<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_1<T>;
    fn outer(self, other: MV1_0<T>) -> MV1_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_1(
            [  a[0]*b[0]]
)
    }
}

impl<T> Mul<MV1_1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0<T>;
    fn mul(self,  other: MV1_1<T>) -> MV1_0<T> {
        let a = self.0;
        let b = other.0;
        MV1_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV1_1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0<T>;
    fn inner(self, other: MV1_1<T>) -> MV1_0<T> {
        let a = self.0;
        let b = other.0;
        MV1_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV1_1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn outer(self, _: MV1_1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> From<MV1_1<T>> for MV1_0_1<T> where T: Zero+Copy {
    fn from(a: MV1_1<T>) -> Self {
        let a = a.0;
        MV1_0_1(
            [T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV1_0_1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn mul(self,  other: MV1_0_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV1_0_1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn inner(self, other: MV1_0_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV1_0_1<T>> for MV1_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_1<T>;
    fn outer(self, other: MV1_0_1<T>) -> MV1_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_1(
            [  a[0]*b[0]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV1_0_1<T> (
    [T;2]
);

impl<T> VectorSpace for MV1_0_1<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV1_0_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV1_0_1<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV1_0_1<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV1_0_1(
            [ self.0[0],
              self.0[1]]
        )
    }
}

impl<T> MV1_0_1<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV1_0_1<T> {
        let mut z = MV1_0_1::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV1_0_1::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV1_0_1<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV1_0_1 (
            [a[0] + b[0],
             a[1] + b[1]]
        )
    }
}

impl<T> Sub for MV1_0_1<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV1_0_1 (
            [a[0] - b[0],
             a[1] - b[1]]
        )
    }
}

impl<T> Neg for MV1_0_1<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV1_0_1 (
            [-a[0],
             -a[1]]
        )
    }
}

impl<T> Mul<T> for MV1_0_1<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV1_0_1 (
            [a[0] * other,
             a[1] * other]
        )
    }
}

impl<T> Div<T> for MV1_0_1<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV1_0_1 (
            [a[0] / other,
             a[1] / other]
        )
    }
}

impl<T> Rem<T> for MV1_0_1<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV1_0_1 (
            [a[0] % other,
             a[1] % other]
        )
    }
}

impl<T> AddAssign for MV1_0_1<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
    }
}

impl<T> SubAssign for MV1_0_1<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
    }
}

impl<T> MulAssign<T> for MV1_0_1<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
    }
}

impl<T> DivAssign<T> for MV1_0_1<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
    }
}

impl<T> Zero for MV1_0_1<T> where T: Zero+Copy {
    fn zero() -> Self { MV1_0_1 ([ T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV1_0_1<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        true
    }
}

impl<T> One for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV1_0_1 ([T::one(),T::zero()]) }
}

impl<T> From<T> for MV1_0_1<T> where T: Zero {
    fn from(a: T) -> Self {
        MV1_0_1 (
            [a,T::zero()]
        )
    }
}

impl<T> From<MV1_0_1<T>> for MV1<T> where T: Zero+Copy {
    fn from(a: MV1_0_1<T>) -> Self {
        MV1(
            []
)
    }
}

impl<T> Mul<MV1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn mul(self,  _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Inner<MV1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn inner(self, _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> Outer<MV1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1<T>;
    fn outer(self, _: MV1<T>) -> MV1<T> {
        MV1(
            []
)
    }
}

impl<T> From<MV1_0_1<T>> for MV1_0<T> where T: Zero+Copy {
    fn from(a: MV1_0_1<T>) -> Self {
        let a = a.0;
        MV1_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV1_0<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn mul(self,  other: MV1_0<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV1_0<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn inner(self, other: MV1_0<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV1_0<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn outer(self, other: MV1_0<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> From<MV1_0_1<T>> for MV1_1<T> where T: Zero+Copy {
    fn from(a: MV1_0_1<T>) -> Self {
        let a = a.0;
        MV1_1(
            [a[1]]
)
    }
}

impl<T> Mul<MV1_1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn mul(self,  other: MV1_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV1_1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn inner(self, other: MV1_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV1_1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_1<T>;
    fn outer(self, other: MV1_1<T>) -> MV1_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_1(
            [  a[0]*b[0]]
)
    }
}

impl<T> Mul<MV1_0_1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn mul(self,  other: MV1_0_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV1_0_1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn inner(self, other: MV1_0_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV1_0_1<T>> for MV1_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV1_0_1<T>;
    fn outer(self, other: MV1_0_1<T>) -> MV1_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV1_0_1(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV2<T> (
    [T;0]
);

impl<T> VectorSpace for MV2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV2(
            []
        )
    }
}
impl<T> MV2<T> where T: Real+AddAssign {
    pub fn exp(self) -> Self {
        MV2([])
    }
}
impl<T> Add for MV2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  _: Self) -> Self {
        MV2 (
            []
        )
    }
}

impl<T> Sub for MV2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  _: Self) -> Self {
        MV2 (
            []
        )
    }
}

impl<T> Neg for MV2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        MV2 (
            []
        )
    }
}

impl<T> Mul<T> for MV2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  _: T) -> Self {
        MV2 (
            []
        )
    }
}

impl<T> Div<T> for MV2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  _: T) -> Self {
        MV2 (
            []
        )
    }
}

impl<T> Rem<T> for MV2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  _: T) -> Self {
        MV2 (
            []
        )
    }
}

impl<T> AddAssign for MV2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, _: Self) {
        
    }
}

impl<T> SubAssign for MV2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, _: Self) {
        
    }
}

impl<T> MulAssign<T> for MV2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, _: T) {
        
    }
}

impl<T> DivAssign<T> for MV2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, _: T) {
        
    }
}

impl<T> Zero for MV2<T> where T: Zero+Copy {
    fn zero() -> Self { MV2 ([]) }
    fn is_zero(&self) -> bool {
        true
    }
}

impl<T> PartialEq for MV2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        true
    }
}

impl<T> Mul<MV2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2<T>> for MV2_0<T> where T: Zero+Copy {
    fn from(a: MV2<T>) -> Self {
        let a = a.0;
        MV2_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV2_0<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2_0<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2_0<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2_0<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2_0<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_0<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2<T>> for MV2_1<T> where T: Zero+Copy {
    fn from(a: MV2<T>) -> Self {
        let a = a.0;
        MV2_1(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_1<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2_1<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2_1<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2_1<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2_1<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_1<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2<T>> for MV2_2<T> where T: Zero+Copy {
    fn from(a: MV2<T>) -> Self {
        let a = a.0;
        MV2_2(
            [T::zero()]
)
    }
}

impl<T> Mul<MV2_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2<T>> for MV2_0_1<T> where T: Zero+Copy {
    fn from(a: MV2<T>) -> Self {
        let a = a.0;
        MV2_0_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_1<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2_0_1<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2_0_1<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2_0_1<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2_0_1<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_0_1<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2<T>> for MV2_0_2<T> where T: Zero+Copy {
    fn from(a: MV2<T>) -> Self {
        let a = a.0;
        MV2_0_2(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2_0_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2_0_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2_0_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2_0_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_0_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2<T>> for MV2_1_2<T> where T: Zero+Copy {
    fn from(a: MV2<T>) -> Self {
        let a = a.0;
        MV2_1_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_1_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2_1_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2_1_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2_1_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2_1_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_1_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2<T>> for MV2_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV2<T>) -> Self {
        let a = a.0;
        MV2_0_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_1_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2_0_1_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2_0_1_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2_0_1_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2_0_1_2<T>> for MV2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_0_1_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV2_0<T> (
    [T;1]
);

impl<T> MV2_0<T> {
    pub fn new(a0: T) -> Self {
        MV2_0 (
            [a0]
        )
    }
}

impl<T> MV2_0<T> where T: One {
    pub fn value(self) -> T where T: Copy {
        self.0[0]
    }
}

impl<T> MV2_0<T> where T: One {
    pub fn unit() -> Self {
        MV2_0 (
            [T::one()]
        )
    }
}

impl<T> VectorSpace for MV2_0<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV2_0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV2_0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV2_0<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV2_0<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV2_0(
            [ self.0[0]]
        )
    }
}

impl<T> MV2_0<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV2_0<T> {
        let mut z = MV2_0::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV2_0::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV2_0<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_0 (
            [a[0] + b[0]]
        )
    }
}

impl<T> Sub for MV2_0<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_0 (
            [a[0] - b[0]]
        )
    }
}

impl<T> Neg for MV2_0<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV2_0 (
            [-a[0]]
        )
    }
}

impl<T> Mul<T> for MV2_0<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV2_0 (
            [a[0] * other]
        )
    }
}

impl<T> Div<T> for MV2_0<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV2_0 (
            [a[0] / other]
        )
    }
}

impl<T> Rem<T> for MV2_0<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV2_0 (
            [a[0] % other]
        )
    }
}

impl<T> AddAssign for MV2_0<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
    }
}

impl<T> SubAssign for MV2_0<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
    }
}

impl<T> MulAssign<T> for MV2_0<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
    }
}

impl<T> DivAssign<T> for MV2_0<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
    }
}

impl<T> Zero for MV2_0<T> where T: Zero+Copy {
    fn zero() -> Self { MV2_0 ([ T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV2_0<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        true
    }
}

impl<T> One for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV2_0 ([T::one()]) }
}

impl<T> From<T> for MV2_0<T> where T: Zero {
    fn from(a: T) -> Self {
        MV2_0 (
            [a]
        )
    }
}

impl<T> From<MV2_0<T>> for MV2<T> where T: Zero+Copy {
    fn from(a: MV2_0<T>) -> Self {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2_0<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0<T>;
    fn mul(self,  other: MV2_0<T>) -> MV2_0<T> {
        let a = self.0;
        let b = other.0;
        MV2_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_0<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0<T>;
    fn inner(self, other: MV2_0<T>) -> MV2_0<T> {
        let a = self.0;
        let b = other.0;
        MV2_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_0<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0<T>;
    fn outer(self, other: MV2_0<T>) -> MV2_0<T> {
        let a = self.0;
        let b = other.0;
        MV2_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV2_0<T>> for MV2_1<T> where T: Zero+Copy {
    fn from(a: MV2_0<T>) -> Self {
        let a = a.0;
        MV2_1(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_1<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn mul(self,  other: MV2_1<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_1<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn inner(self, other: MV2_1<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_1<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn outer(self, other: MV2_1<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> From<MV2_0<T>> for MV2_2<T> where T: Zero+Copy {
    fn from(a: MV2_0<T>) -> Self {
        let a = a.0;
        MV2_2(
            [T::zero()]
)
    }
}

impl<T> Mul<MV2_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn mul(self,  other: MV2_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn inner(self, other: MV2_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV2_0<T>> for MV2_0_1<T> where T: Zero+Copy {
    fn from(a: MV2_0<T>) -> Self {
        let a = a.0;
        MV2_0_1(
            [a[0],
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_1<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn mul(self,  other: MV2_0_1<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_0_1<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_0_1<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV2_0_1<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn outer(self, other: MV2_0_1<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV2_0<T>> for MV2_0_2<T> where T: Zero+Copy {
    fn from(a: MV2_0<T>) -> Self {
        let a = a.0;
        MV2_0_2(
            [a[0],
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn mul(self,  other: MV2_0_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_0_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn inner(self, other: MV2_0_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_0_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn outer(self, other: MV2_0_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> From<MV2_0<T>> for MV2_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_0<T>) -> Self {
        let a = a.0;
        MV2_1_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_1_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn mul(self,  other: MV2_1_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_1_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn inner(self, other: MV2_1_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV2_1_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_1_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV2_0<T>> for MV2_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_0<T>) -> Self {
        let a = a.0;
        MV2_0_1_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_1_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV2_0_1_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV2_0_1_2<T>> for MV2_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV2_1<T> (
    [T;2]
);

impl<T> MV2_1<T> {
    pub fn new(a0: T,a1: T) -> Self {
        MV2_1 (
            [a0,
             a1]
        )
    }
}

impl<T> MV2_1<T> where T: Zero+One {
    pub fn vector(self) -> [T;2] {
        self.0
    }
    pub fn e1() -> Self {
        MV2_1 (
            [T::one(),T::zero()]
        )
    }
    pub fn e2() -> Self {
        MV2_1 (
            [T::zero(),T::one()]
        )
    }
}

impl<T> VectorSpace for MV2_1<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV2_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV2_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV2_1<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV2_1<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV2_1(
            [ self.0[0],
              self.0[1]]
        )
    }
}

impl<T> MV2_1<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV2_0_1_2<T> {
        let mut z = MV2_0_1_2::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV2_0_1_2::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV2_1<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_1 (
            [a[0] + b[0],
             a[1] + b[1]]
        )
    }
}

impl<T> Sub for MV2_1<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_1 (
            [a[0] - b[0],
             a[1] - b[1]]
        )
    }
}

impl<T> Neg for MV2_1<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV2_1 (
            [-a[0],
             -a[1]]
        )
    }
}

impl<T> Mul<T> for MV2_1<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV2_1 (
            [a[0] * other,
             a[1] * other]
        )
    }
}

impl<T> Div<T> for MV2_1<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV2_1 (
            [a[0] / other,
             a[1] / other]
        )
    }
}

impl<T> Rem<T> for MV2_1<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV2_1 (
            [a[0] % other,
             a[1] % other]
        )
    }
}

impl<T> AddAssign for MV2_1<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
    }
}

impl<T> SubAssign for MV2_1<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
    }
}

impl<T> MulAssign<T> for MV2_1<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
    }
}

impl<T> DivAssign<T> for MV2_1<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
    }
}

impl<T> Zero for MV2_1<T> where T: Zero+Copy {
    fn zero() -> Self { MV2_1 ([ T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV2_1<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        true
    }
}

impl<T> From<MV2_1<T>> for MV2<T> where T: Zero+Copy {
    fn from(a: MV2_1<T>) -> Self {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_1<T>> for MV2_0<T> where T: Zero+Copy {
    fn from(a: MV2_1<T>) -> Self {
        let a = a.0;
        MV2_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV2_0<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn mul(self,  other: MV2_0<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV2_0<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn inner(self, other: MV2_0<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV2_0<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn outer(self, other: MV2_0<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Mul<MV2_1<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn mul(self,  other: MV2_1<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [  a[1]*b[1] + a[0]*b[0],
             - a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_1<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0<T>;
    fn inner(self, other: MV2_1<T>) -> MV2_0<T> {
        let a = self.0;
        let b = other.0;
        MV2_0(
            [  a[1]*b[1] + a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_1<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_1<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [- a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV2_1<T>> for MV2_2<T> where T: Zero+Copy {
    fn from(a: MV2_1<T>) -> Self {
        let a = a.0;
        MV2_2(
            [T::zero()]
)
    }
}

impl<T> Mul<MV2_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn mul(self,  other: MV2_2<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [- a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn inner(self, other: MV2_2<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [- a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_1<T>> for MV2_0_1<T> where T: Zero+Copy {
    fn from(a: MV2_1<T>) -> Self {
        let a = a.0;
        MV2_0_1(
            [T::zero(),
             a[0],
             a[1]]
)
    }
}

impl<T> Mul<MV2_0_1<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
             - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_0_1<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_0_1<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_1<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_0_1<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
             - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV2_1<T>> for MV2_0_2<T> where T: Zero+Copy {
    fn from(a: MV2_1<T>) -> Self {
        let a = a.0;
        MV2_0_2(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn mul(self,  other: MV2_0_2<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [- a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_0_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn inner(self, other: MV2_0_2<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [- a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_0_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn outer(self, other: MV2_0_2<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> From<MV2_1<T>> for MV2_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_1<T>) -> Self {
        let a = a.0;
        MV2_1_2(
            [a[0],
             a[1],
             T::zero()]
)
    }
}

impl<T> Mul<MV2_1_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[1]*b[1] + a[0]*b[0],
             - a[1]*b[2],
               a[0]*b[2],
             - a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_1_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_1_2<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[1]*b[1] + a[0]*b[0],
             - a[1]*b[2],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV2_1_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_1_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [- a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV2_1<T>> for MV2_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_1<T>) -> Self {
        let a = a.0;
        MV2_0_1_2(
            [T::zero(),
             a[0],
             a[1],
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_1_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[1]*b[2] + a[0]*b[1],
             - a[1]*b[3] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[3],
             - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_0_1_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_0_1_2<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[1]*b[2] + a[0]*b[1],
             - a[1]*b[3] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Outer<MV2_0_1_2<T>> for MV2_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_0_1_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
             - a[1]*b[1] + a[0]*b[2]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV2_2<T> (
    [T;1]
);

impl<T> MV2_2<T> {
    pub fn new(a0: T) -> Self {
        MV2_2 (
            [a0]
        )
    }
}

impl<T> MV2_2<T> where T: One {
    pub fn unit() -> Self {
        MV2_2 (
            [T::one()]
        )
    }
}

impl<T> VectorSpace for MV2_2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV2_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV2_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV2_2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV2_2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV2_2(
            [-self.0[0]]
        )
    }
}

impl<T> MV2_2<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV2_0_2<T> {
        let mut z = MV2_0_2::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV2_0_2::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV2_2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_2 (
            [a[0] + b[0]]
        )
    }
}

impl<T> Sub for MV2_2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_2 (
            [a[0] - b[0]]
        )
    }
}

impl<T> Neg for MV2_2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV2_2 (
            [-a[0]]
        )
    }
}

impl<T> Mul<T> for MV2_2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV2_2 (
            [a[0] * other]
        )
    }
}

impl<T> Div<T> for MV2_2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV2_2 (
            [a[0] / other]
        )
    }
}

impl<T> Rem<T> for MV2_2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV2_2 (
            [a[0] % other]
        )
    }
}

impl<T> AddAssign for MV2_2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
    }
}

impl<T> SubAssign for MV2_2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
    }
}

impl<T> MulAssign<T> for MV2_2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
    }
}

impl<T> DivAssign<T> for MV2_2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
    }
}

impl<T> Zero for MV2_2<T> where T: Zero+Copy {
    fn zero() -> Self { MV2_2 ([ T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV2_2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        true
    }
}

impl<T> From<MV2_2<T>> for MV2<T> where T: Zero+Copy {
    fn from(a: MV2_2<T>) -> Self {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_2<T>> for MV2_0<T> where T: Zero+Copy {
    fn from(a: MV2_2<T>) -> Self {
        let a = a.0;
        MV2_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV2_0<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn mul(self,  other: MV2_0<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_0<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn inner(self, other: MV2_0<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_0<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_0<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV2_2<T>> for MV2_1<T> where T: Zero+Copy {
    fn from(a: MV2_2<T>) -> Self {
        let a = a.0;
        MV2_1(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_1<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn mul(self,  other: MV2_1<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[1],
             - a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_1<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn inner(self, other: MV2_1<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[1],
             - a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_1<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_1<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0<T>;
    fn mul(self,  other: MV2_2<T>) -> MV2_0<T> {
        let a = self.0;
        let b = other.0;
        MV2_0(
            [- a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0<T>;
    fn inner(self, other: MV2_2<T>) -> MV2_0<T> {
        let a = self.0;
        let b = other.0;
        MV2_0(
            [- a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_2<T>> for MV2_0_1<T> where T: Zero+Copy {
    fn from(a: MV2_2<T>) -> Self {
        let a = a.0;
        MV2_0_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_1<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn mul(self,  other: MV2_0_1<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_0_1<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn inner(self, other: MV2_0_1<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_1<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_0_1<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV2_2<T>> for MV2_0_2<T> where T: Zero+Copy {
    fn from(a: MV2_2<T>) -> Self {
        let a = a.0;
        MV2_0_2(
            [T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV2_0_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn mul(self,  other: MV2_0_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [- a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_0_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn inner(self, other: MV2_0_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [- a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_0_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV2_2<T>> for MV2_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_2<T>) -> Self {
        let a = a.0;
        MV2_1_2(
            [T::zero(),
             T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV2_1_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn mul(self,  other: MV2_1_2<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [- a[0]*b[2],
               a[0]*b[1],
             - a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_1_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_1_2<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [- a[0]*b[2],
               a[0]*b[1],
             - a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_1_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_1_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_2<T>> for MV2_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_2<T>) -> Self {
        let a = a.0;
        MV2_0_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV2_0_1_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_0_1_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_1_2<T>> for MV2_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_0_1_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV2_0_1<T> (
    [T;3]
);

impl<T> VectorSpace for MV2_0_1<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV2_0_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV2_0_1<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV2_0_1<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV2_0_1(
            [ self.0[0],
              self.0[1],
              self.0[2]]
        )
    }
}

impl<T> MV2_0_1<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV2_0_1_2<T> {
        let mut z = MV2_0_1_2::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV2_0_1_2::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV2_0_1<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_0_1 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2]]
        )
    }
}

impl<T> Sub for MV2_0_1<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_0_1 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2]]
        )
    }
}

impl<T> Neg for MV2_0_1<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV2_0_1 (
            [-a[0],
             -a[1],
             -a[2]]
        )
    }
}

impl<T> Mul<T> for MV2_0_1<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_1 (
            [a[0] * other,
             a[1] * other,
             a[2] * other]
        )
    }
}

impl<T> Div<T> for MV2_0_1<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_1 (
            [a[0] / other,
             a[1] / other,
             a[2] / other]
        )
    }
}

impl<T> Rem<T> for MV2_0_1<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_1 (
            [a[0] % other,
             a[1] % other,
             a[2] % other]
        )
    }
}

impl<T> AddAssign for MV2_0_1<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
    }
}

impl<T> SubAssign for MV2_0_1<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
    }
}

impl<T> MulAssign<T> for MV2_0_1<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
    }
}

impl<T> DivAssign<T> for MV2_0_1<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
    }
}

impl<T> Zero for MV2_0_1<T> where T: Zero+Copy {
    fn zero() -> Self { MV2_0_1 ([ T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV2_0_1<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        true
    }
}

impl<T> From<T> for MV2_0_1<T> where T: Zero {
    fn from(a: T) -> Self {
        MV2_0_1 (
            [a,T::zero(),T::zero()]
        )
    }
}

impl<T> From<MV2_0_1<T>> for MV2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1<T>) -> Self {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_0_1<T>> for MV2_0<T> where T: Zero+Copy {
    fn from(a: MV2_0_1<T>) -> Self {
        let a = a.0;
        MV2_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV2_0<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn mul(self,  other: MV2_0<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Inner<MV2_0<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_0<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV2_0<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn outer(self, other: MV2_0<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> From<MV2_0_1<T>> for MV2_1<T> where T: Zero+Copy {
    fn from(a: MV2_0_1<T>) -> Self {
        let a = a.0;
        MV2_1(
            [a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV2_1<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
             - a[2]*b[0] + a[1]*b[1]]
)
    }
}

impl<T> Inner<MV2_1<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_1<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_1<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_1<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
             - a[2]*b[0] + a[1]*b[1]]
)
    }
}

impl<T> From<MV2_0_1<T>> for MV2_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1<T>) -> Self {
        let a = a.0;
        MV2_2(
            [T::zero()]
)
    }
}

impl<T> Mul<MV2_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn mul(self,  other: MV2_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [- a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn inner(self, other: MV2_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [- a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> Mul<MV2_0_1<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV2_0_1<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_0_1<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2]]
)
    }
}

impl<T> Outer<MV2_0_1<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV2_0_1<T>> for MV2_0_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1<T>) -> Self {
        let a = a.0;
        MV2_0_2(
            [a[0],
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0],
               a[2]*b[0] + a[1]*b[1],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_0_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0],
               a[2]*b[0] + a[1]*b[1],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_0_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> From<MV2_0_1<T>> for MV2_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1<T>) -> Self {
        let a = a.0;
        MV2_1_2(
            [a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV2_1_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[1] + a[1]*b[0],
             - a[2]*b[2] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_1_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[1] + a[1]*b[0],
             - a[2]*b[2] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV2_1_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_1_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV2_0_1<T>> for MV2_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1<T>) -> Self {
        let a = a.0;
        MV2_0_1_2(
            [a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_1_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV2_0_1_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV2_0_1_2<T>> for MV2_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV2_0_2<T> (
    [T;2]
);

impl<T> VectorSpace for MV2_0_2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV2_0_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV2_0_2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV2_0_2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV2_0_2(
            [ self.0[0],
             -self.0[1]]
        )
    }
}

impl<T> MV2_0_2<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV2_0_2<T> {
        let mut z = MV2_0_2::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV2_0_2::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV2_0_2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_0_2 (
            [a[0] + b[0],
             a[1] + b[1]]
        )
    }
}

impl<T> Sub for MV2_0_2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_0_2 (
            [a[0] - b[0],
             a[1] - b[1]]
        )
    }
}

impl<T> Neg for MV2_0_2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV2_0_2 (
            [-a[0],
             -a[1]]
        )
    }
}

impl<T> Mul<T> for MV2_0_2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_2 (
            [a[0] * other,
             a[1] * other]
        )
    }
}

impl<T> Div<T> for MV2_0_2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_2 (
            [a[0] / other,
             a[1] / other]
        )
    }
}

impl<T> Rem<T> for MV2_0_2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_2 (
            [a[0] % other,
             a[1] % other]
        )
    }
}

impl<T> AddAssign for MV2_0_2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
    }
}

impl<T> SubAssign for MV2_0_2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
    }
}

impl<T> MulAssign<T> for MV2_0_2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
    }
}

impl<T> DivAssign<T> for MV2_0_2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
    }
}

impl<T> Zero for MV2_0_2<T> where T: Zero+Copy {
    fn zero() -> Self { MV2_0_2 ([ T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV2_0_2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        true
    }
}

impl<T> One for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV2_0_2 ([T::one(),T::zero()]) }
}

impl<T> From<T> for MV2_0_2<T> where T: Zero {
    fn from(a: T) -> Self {
        MV2_0_2 (
            [a,T::zero()]
        )
    }
}

impl<T> From<MV2_0_2<T>> for MV2<T> where T: Zero+Copy {
    fn from(a: MV2_0_2<T>) -> Self {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_0_2<T>> for MV2_0<T> where T: Zero+Copy {
    fn from(a: MV2_0_2<T>) -> Self {
        let a = a.0;
        MV2_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV2_0<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn mul(self,  other: MV2_0<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV2_0<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn inner(self, other: MV2_0<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV2_0<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn outer(self, other: MV2_0<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> From<MV2_0_2<T>> for MV2_1<T> where T: Zero+Copy {
    fn from(a: MV2_0_2<T>) -> Self {
        let a = a.0;
        MV2_1(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_1<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn mul(self,  other: MV2_1<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[1]*b[1] + a[0]*b[0],
             - a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_1<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn inner(self, other: MV2_1<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[1]*b[1] + a[0]*b[0],
             - a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_1<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1<T>;
    fn outer(self, other: MV2_1<T>) -> MV2_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_1(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> From<MV2_0_2<T>> for MV2_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_2<T>) -> Self {
        let a = a.0;
        MV2_2(
            [a[1]]
)
    }
}

impl<T> Mul<MV2_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn mul(self,  other: MV2_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [- a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn inner(self, other: MV2_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [- a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV2_0_2<T>> for MV2_0_1<T> where T: Zero+Copy {
    fn from(a: MV2_0_2<T>) -> Self {
        let a = a.0;
        MV2_0_1(
            [a[0],
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV2_0_1<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
             - a[1]*b[1] + a[0]*b[2],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV2_0_1<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
             - a[1]*b[1] + a[0]*b[2],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_1<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[1]*b[0]]
)
    }
}

impl<T> Mul<MV2_0_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn mul(self,  other: MV2_0_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [- a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_0_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn inner(self, other: MV2_0_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [- a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_0_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_2<T>;
    fn outer(self, other: MV2_0_2<T>) -> MV2_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_2(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV2_0_2<T>> for MV2_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_2<T>) -> Self {
        let a = a.0;
        MV2_1_2(
            [T::zero(),
             T::zero(),
             a[1]]
)
    }
}

impl<T> Mul<MV2_1_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[1]*b[2],
               a[1]*b[1] + a[0]*b[0],
             - a[1]*b[0] + a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_1_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[1]*b[2],
               a[1]*b[1] + a[0]*b[0],
             - a[1]*b[0] + a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV2_1_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_1_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV2_0_2<T>> for MV2_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_2<T>) -> Self {
        let a = a.0;
        MV2_0_1_2(
            [a[0],
             T::zero(),
             T::zero(),
             a[1]]
)
    }
}

impl<T> Mul<MV2_0_1_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[1]*b[3] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
             - a[1]*b[1] + a[0]*b[2],
               a[1]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV2_0_1_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[1]*b[3] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
             - a[1]*b[1] + a[0]*b[2],
               a[1]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Outer<MV2_0_1_2<T>> for MV2_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[1]*b[0] + a[0]*b[3]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV2_1_2<T> (
    [T;3]
);

impl<T> VectorSpace for MV2_1_2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV2_1_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV2_1_2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV2_1_2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV2_1_2(
            [ self.0[0],
              self.0[1],
             -self.0[2]]
        )
    }
}

impl<T> MV2_1_2<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV2_0_1_2<T> {
        let mut z = MV2_0_1_2::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV2_0_1_2::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV2_1_2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_1_2 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2]]
        )
    }
}

impl<T> Sub for MV2_1_2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_1_2 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2]]
        )
    }
}

impl<T> Neg for MV2_1_2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV2_1_2 (
            [-a[0],
             -a[1],
             -a[2]]
        )
    }
}

impl<T> Mul<T> for MV2_1_2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV2_1_2 (
            [a[0] * other,
             a[1] * other,
             a[2] * other]
        )
    }
}

impl<T> Div<T> for MV2_1_2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV2_1_2 (
            [a[0] / other,
             a[1] / other,
             a[2] / other]
        )
    }
}

impl<T> Rem<T> for MV2_1_2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV2_1_2 (
            [a[0] % other,
             a[1] % other,
             a[2] % other]
        )
    }
}

impl<T> AddAssign for MV2_1_2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
    }
}

impl<T> SubAssign for MV2_1_2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
    }
}

impl<T> MulAssign<T> for MV2_1_2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
    }
}

impl<T> DivAssign<T> for MV2_1_2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
    }
}

impl<T> Zero for MV2_1_2<T> where T: Zero+Copy {
    fn zero() -> Self { MV2_1_2 ([ T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV2_1_2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        true
    }
}

impl<T> From<MV2_1_2<T>> for MV2<T> where T: Zero+Copy {
    fn from(a: MV2_1_2<T>) -> Self {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_1_2<T>> for MV2_0<T> where T: Zero+Copy {
    fn from(a: MV2_1_2<T>) -> Self {
        let a = a.0;
        MV2_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV2_0<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn mul(self,  other: MV2_0<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Inner<MV2_0<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn inner(self, other: MV2_0<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV2_0<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_0<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> From<MV2_1_2<T>> for MV2_1<T> where T: Zero+Copy {
    fn from(a: MV2_1_2<T>) -> Self {
        let a = a.0;
        MV2_1(
            [a[0],
             a[1]]
)
    }
}

impl<T> Mul<MV2_1<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[1]*b[1] + a[0]*b[0],
               a[2]*b[1],
             - a[2]*b[0],
             - a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_1<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_1<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[1]*b[1] + a[0]*b[0],
               a[2]*b[1],
             - a[2]*b[0]]
)
    }
}

impl<T> Outer<MV2_1<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_1<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [- a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV2_1_2<T>> for MV2_2<T> where T: Zero+Copy {
    fn from(a: MV2_1_2<T>) -> Self {
        let a = a.0;
        MV2_2(
            [a[2]]
)
    }
}

impl<T> Mul<MV2_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn mul(self,  other: MV2_2<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [- a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_2<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [- a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2_2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_1_2<T>> for MV2_0_1<T> where T: Zero+Copy {
    fn from(a: MV2_1_2<T>) -> Self {
        let a = a.0;
        MV2_0_1(
            [T::zero(),
             a[0],
             a[1]]
)
    }
}

impl<T> Mul<MV2_0_1<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_0_1<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_1<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_0_1<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV2_1_2<T>> for MV2_0_2<T> where T: Zero+Copy {
    fn from(a: MV2_1_2<T>) -> Self {
        let a = a.0;
        MV2_0_2(
            [T::zero(),
             a[2]]
)
    }
}

impl<T> Mul<MV2_0_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[2]*b[1],
             - a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0]]
)
    }
}

impl<T> Inner<MV2_0_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[2]*b[1],
             - a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_0_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Mul<MV2_1_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[2]*b[1] - a[1]*b[2],
             - a[2]*b[0] + a[0]*b[2],
             - a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_1_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_1_2<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [- a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[2]*b[1] - a[1]*b[2],
             - a[2]*b[0] + a[0]*b[2]]
)
    }
}

impl<T> Outer<MV2_1_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_1_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [- a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV2_1_2<T>> for MV2_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_1_2<T>) -> Self {
        let a = a.0;
        MV2_0_1_2(
            [T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV2_0_1_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] - a[1]*b[3] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0] + a[0]*b[3],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_0_1_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] - a[1]*b[3] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0] + a[0]*b[3],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_1_2<T>> for MV2_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_0_1_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV2_0_1_2<T> (
    [T;4]
);

impl<T> VectorSpace for MV2_0_1_2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV2_0_1_2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV2_0_1_2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV2_0_1_2(
            [ self.0[0],
              self.0[1],
              self.0[2],
             -self.0[3]]
        )
    }
}

impl<T> MV2_0_1_2<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV2_0_1_2<T> {
        let mut z = MV2_0_1_2::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV2_0_1_2::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV2_0_1_2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3]]
        )
    }
}

impl<T> Sub for MV2_0_1_2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3]]
        )
    }
}

impl<T> Neg for MV2_0_1_2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV2_0_1_2 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3]]
        )
    }
}

impl<T> Mul<T> for MV2_0_1_2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_1_2 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other]
        )
    }
}

impl<T> Div<T> for MV2_0_1_2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_1_2 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other]
        )
    }
}

impl<T> Rem<T> for MV2_0_1_2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV2_0_1_2 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other]
        )
    }
}

impl<T> AddAssign for MV2_0_1_2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
    }
}

impl<T> SubAssign for MV2_0_1_2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
    }
}

impl<T> MulAssign<T> for MV2_0_1_2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
    }
}

impl<T> DivAssign<T> for MV2_0_1_2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
    }
}

impl<T> Zero for MV2_0_1_2<T> where T: Zero+Copy {
    fn zero() -> Self { MV2_0_1_2 ([ T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV2_0_1_2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        true
    }
}

impl<T> One for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV2_0_1_2 ([T::one(),T::zero(),T::zero(),T::zero()]) }
}

impl<T> From<T> for MV2_0_1_2<T> where T: Zero {
    fn from(a: T) -> Self {
        MV2_0_1_2 (
            [a,T::zero(),T::zero(),T::zero()]
        )
    }
}

impl<T> From<MV2_0_1_2<T>> for MV2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1_2<T>) -> Self {
        MV2(
            []
)
    }
}

impl<T> Mul<MV2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn mul(self,  _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Inner<MV2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn inner(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> Outer<MV2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2<T>;
    fn outer(self, _: MV2<T>) -> MV2<T> {
        MV2(
            []
)
    }
}

impl<T> From<MV2_0_1_2<T>> for MV2_0<T> where T: Zero+Copy {
    fn from(a: MV2_0_1_2<T>) -> Self {
        let a = a.0;
        MV2_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV2_0<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV2_0<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV2_0<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV2_0_1_2<T>> for MV2_1<T> where T: Zero+Copy {
    fn from(a: MV2_0_1_2<T>) -> Self {
        let a = a.0;
        MV2_1(
            [a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV2_1<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[1] + a[1]*b[0],
               a[3]*b[1] + a[0]*b[0],
             - a[3]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[1]*b[1]]
)
    }
}

impl<T> Inner<MV2_1<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1<T>;
    fn inner(self, other: MV2_1<T>) -> MV2_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1(
            [  a[2]*b[1] + a[1]*b[0],
               a[3]*b[1] + a[0]*b[0],
             - a[3]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_1<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_1<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
             - a[2]*b[0] + a[1]*b[1]]
)
    }
}

impl<T> From<MV2_0_1_2<T>> for MV2_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1_2<T>) -> Self {
        let a = a.0;
        MV2_2(
            [a[3]]
)
    }
}

impl<T> Mul<MV2_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV2_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV2_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_2<T>;
    fn outer(self, other: MV2_2<T>) -> MV2_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_2(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV2_0_1_2<T>> for MV2_0_1<T> where T: Zero+Copy {
    fn from(a: MV2_0_1_2<T>) -> Self {
        let a = a.0;
        MV2_0_1(
            [a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV2_0_1<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV2_0_1<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[0]*b[2],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV2_0_1<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_1<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV2_0_1_2<T>> for MV2_0_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1_2<T>) -> Self {
        let a = a.0;
        MV2_0_2(
            [a[0],
             a[3]]
)
    }
}

impl<T> Mul<MV2_0_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[3]*b[1] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0],
               a[2]*b[0] + a[1]*b[1],
               a[3]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV2_0_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[3]*b[1] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0],
               a[2]*b[0] + a[1]*b[1],
               a[3]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV2_0_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV2_0_1_2<T>> for MV2_1_2<T> where T: Zero+Copy {
    fn from(a: MV2_0_1_2<T>) -> Self {
        let a = a.0;
        MV2_1_2(
            [a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV2_1_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV2_1_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV2_1_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_1_2<T>;
    fn outer(self, other: MV2_1_2<T>) -> MV2_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Mul<MV2_0_1_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn mul(self,  other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV2_0_1_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn inner(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [- a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Outer<MV2_0_1_2<T>> for MV2_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV2_0_1_2<T>;
    fn outer(self, other: MV2_0_1_2<T>) -> MV2_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV2_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3<T> (
    [T;0]
);

impl<T> VectorSpace for MV3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3(
            []
        )
    }
}
impl<T> MV3<T> where T: Real+AddAssign {
    pub fn exp(self) -> Self {
        MV3([])
    }
}
impl<T> Add for MV3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  _: Self) -> Self {
        MV3 (
            []
        )
    }
}

impl<T> Sub for MV3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  _: Self) -> Self {
        MV3 (
            []
        )
    }
}

impl<T> Neg for MV3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        MV3 (
            []
        )
    }
}

impl<T> Mul<T> for MV3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  _: T) -> Self {
        MV3 (
            []
        )
    }
}

impl<T> Div<T> for MV3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  _: T) -> Self {
        MV3 (
            []
        )
    }
}

impl<T> Rem<T> for MV3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  _: T) -> Self {
        MV3 (
            []
        )
    }
}

impl<T> AddAssign for MV3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, _: Self) {
        
    }
}

impl<T> SubAssign for MV3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, _: Self) {
        
    }
}

impl<T> MulAssign<T> for MV3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, _: T) {
        
    }
}

impl<T> DivAssign<T> for MV3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, _: T) {
        
    }
}

impl<T> Zero for MV3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3 ([]) }
    fn is_zero(&self) -> bool {
        true
    }
}

impl<T> PartialEq for MV3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        true
    }
}

impl<T> Mul<MV3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_0<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_0<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_0<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_1<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_1<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_1<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_3(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_0_1<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_0_1<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_0_1<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_0_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_0_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_0_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_0_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_0_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_0_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_1_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_1_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_1_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_1_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_1_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_1_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_0_1_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_0_1_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_0_1_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_0_1_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_0_1_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_0_1_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_0_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_0_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_0_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_1_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_1_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_1_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3_0_1_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3_0_1_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_0_1_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_0<T> (
    [T;1]
);

impl<T> MV3_0<T> {
    pub fn new(a0: T) -> Self {
        MV3_0 (
            [a0]
        )
    }
}

impl<T> MV3_0<T> where T: One {
    pub fn value(self) -> T where T: Copy {
        self.0[0]
    }
}

impl<T> MV3_0<T> where T: One {
    pub fn unit() -> Self {
        MV3_0 (
            [T::one()]
        )
    }
}

impl<T> VectorSpace for MV3_0<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_0<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_0<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_0<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_0(
            [ self.0[0]]
        )
    }
}

impl<T> MV3_0<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0<T> {
        let mut z = MV3_0::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_0<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0 (
            [a[0] + b[0]]
        )
    }
}

impl<T> Sub for MV3_0<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0 (
            [a[0] - b[0]]
        )
    }
}

impl<T> Neg for MV3_0<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_0 (
            [-a[0]]
        )
    }
}

impl<T> Mul<T> for MV3_0<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_0 (
            [a[0] * other]
        )
    }
}

impl<T> Div<T> for MV3_0<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_0 (
            [a[0] / other]
        )
    }
}

impl<T> Rem<T> for MV3_0<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_0 (
            [a[0] % other]
        )
    }
}

impl<T> AddAssign for MV3_0<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
    }
}

impl<T> SubAssign for MV3_0<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
    }
}

impl<T> MulAssign<T> for MV3_0<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
    }
}

impl<T> DivAssign<T> for MV3_0<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
    }
}

impl<T> Zero for MV3_0<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_0 ([ T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_0<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        true
    }
}

impl<T> One for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV3_0 ([T::one()]) }
}

impl<T> From<T> for MV3_0<T> where T: Zero {
    fn from(a: T) -> Self {
        MV3_0 (
            [a]
        )
    }
}

impl<T> From<MV3_0<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_0<T> {
        let a = self.0;
        let b = other.0;
        MV3_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_0<T> {
        let a = self.0;
        let b = other.0;
        MV3_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_0<T> {
        let a = self.0;
        let b = other.0;
        MV3_0(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_3(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [a[0],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [  a[0]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> From<MV3_0<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6],
               a[0]*b[7]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6],
               a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_0<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6],
               a[0]*b[7]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_1<T> (
    [T;3]
);

impl<T> MV3_1<T> {
    pub fn new(a0: T,a1: T,a2: T) -> Self {
        MV3_1 (
            [a0,
             a1,
             a2]
        )
    }
}

impl<T> MV3_1<T> where T: Zero+One {
    pub fn vector(self) -> [T;3] {
        self.0
    }
    pub fn e1() -> Self {
        MV3_1 (
            [T::one(),T::zero(),T::zero()]
        )
    }
    pub fn e2() -> Self {
        MV3_1 (
            [T::zero(),T::one(),T::zero()]
        )
    }
    pub fn e3() -> Self {
        MV3_1 (
            [T::zero(),T::zero(),T::one()]
        )
    }
}

impl<T> VectorSpace for MV3_1<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_1<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_1<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_1(
            [ self.0[0],
              self.0[1],
              self.0[2]]
        )
    }
}

impl<T> MV3_1<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_1<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_1 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2]]
        )
    }
}

impl<T> Sub for MV3_1<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_1 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2]]
        )
    }
}

impl<T> Neg for MV3_1<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_1 (
            [-a[0],
             -a[1],
             -a[2]]
        )
    }
}

impl<T> Mul<T> for MV3_1<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_1 (
            [a[0] * other,
             a[1] * other,
             a[2] * other]
        )
    }
}

impl<T> Div<T> for MV3_1<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_1 (
            [a[0] / other,
             a[1] / other,
             a[2] / other]
        )
    }
}

impl<T> Rem<T> for MV3_1<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_1 (
            [a[0] % other,
             a[1] % other,
             a[2] % other]
        )
    }
}

impl<T> AddAssign for MV3_1<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
    }
}

impl<T> SubAssign for MV3_1<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
    }
}

impl<T> MulAssign<T> for MV3_1<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
    }
}

impl<T> DivAssign<T> for MV3_1<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
    }
}

impl<T> Zero for MV3_1<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_1 ([ T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_1<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        true
    }
}

impl<T> From<MV3_1<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_1<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_0<T> {
        let a = self.0;
        let b = other.0;
        MV3_0(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[2]*b[1] - a[1]*b[0],
             - a[2]*b[2] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [- a[2]*b[1] - a[1]*b[0],
             - a[2]*b[2] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_3(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_1<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [- a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[1]*b[3] + a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1],
             - a[1]*b[1],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1],
             - a[1]*b[1],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [a[0],
             a[1],
             a[2],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[2]*b[4] - a[1]*b[3],
             - a[2]*b[5] + a[0]*b[3],
               a[1]*b[5] + a[0]*b[4],
             - a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[2]*b[4] - a[1]*b[3],
             - a[2]*b[5] + a[0]*b[3],
               a[1]*b[5] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[2]*b[3] - a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] - a[1]*b[3] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[2]*b[1] - a[1]*b[0],
             - a[2]*b[2] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[3],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[2]*b[1] - a[1]*b[0],
             - a[2]*b[2] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3],
               a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[1]*b[6] + a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3],
               a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[4] - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] - a[1]*b[4] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[4],
             - a[1]*b[4],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[4],
             - a[1]*b[4],
               a[0]*b[4],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[4],
             - a[1]*b[4],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [a[0],
             a[1],
             a[2],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[2]*b[4] - a[1]*b[3],
             - a[2]*b[5] + a[0]*b[3],
               a[1]*b[5] + a[0]*b[4],
               a[2]*b[6] - a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] - a[1]*b[6] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[6],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[2]*b[4] - a[1]*b[3],
             - a[2]*b[5] + a[0]*b[3],
               a[1]*b[5] + a[0]*b[4],
               a[2]*b[6],
             - a[1]*b[6],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> From<MV3_1<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[2]*b[7] - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] - a[1]*b[7] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3] + a[0]*b[7],
               a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[2]*b[7],
             - a[1]*b[7],
               a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3],
               a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_2<T> (
    [T;3]
);

impl<T> MV3_2<T> {
    pub fn new(a0: T,a1: T,a2: T) -> Self {
        MV3_2 (
            [a0,
             a1,
             a2]
        )
    }
}

impl<T> VectorSpace for MV3_2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_2(
            [-self.0[0],
             -self.0[1],
             -self.0[2]]
        )
    }
}

impl<T> MV3_2<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_2<T> {
        let mut z = MV3_0_2::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_2::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_2 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2]]
        )
    }
}

impl<T> Sub for MV3_2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_2 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2]]
        )
    }
}

impl<T> Neg for MV3_2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_2 (
            [-a[0],
             -a[1],
             -a[2]]
        )
    }
}

impl<T> Mul<T> for MV3_2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_2 (
            [a[0] * other,
             a[1] * other,
             a[2] * other]
        )
    }
}

impl<T> Div<T> for MV3_2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_2 (
            [a[0] / other,
             a[1] / other,
             a[2] / other]
        )
    }
}

impl<T> Rem<T> for MV3_2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_2 (
            [a[0] % other,
             a[1] % other,
             a[2] % other]
        )
    }
}

impl<T> AddAssign for MV3_2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
    }
}

impl<T> SubAssign for MV3_2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
    }
}

impl<T> MulAssign<T> for MV3_2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
    }
}

impl<T> DivAssign<T> for MV3_2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
    }
}

impl<T> Zero for MV3_2<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_2 ([ T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        true
    }
}

impl<T> From<MV3_2<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_2<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[2]*b[2] - a[1]*b[1] - a[0]*b[0],
               a[2]*b[1] - a[1]*b[2],
             - a[2]*b[0] + a[0]*b[2],
               a[1]*b[0] - a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_0<T> {
        let a = self.0;
        let b = other.0;
        MV3_0(
            [- a[2]*b[2] - a[1]*b[1] - a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_2<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_3(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [- a[2]*b[0],
               a[1]*b[0],
             - a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [- a[2]*b[0],
               a[1]*b[0],
             - a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_2<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[2]*b[3] - a[1]*b[2] - a[0]*b[1],
               a[2]*b[2] - a[1]*b[3] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0] + a[0]*b[3],
               a[2]*b[0] + a[1]*b[1] - a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[2]*b[3] - a[1]*b[2] - a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[2]*b[1],
               a[1]*b[1],
             - a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[2]*b[1],
               a[1]*b[1],
             - a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[5] - a[1]*b[4] - a[0]*b[3],
               a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0],
               a[2]*b[4] - a[1]*b[5],
             - a[2]*b[3] + a[0]*b[5],
               a[1]*b[3] - a[0]*b[4],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[2]*b[5] - a[1]*b[4] - a[0]*b[3],
               a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] + a[1]*b[3] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0] - a[0]*b[3],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [- a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] + a[1]*b[3] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0] - a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[2]*b[2] - a[1]*b[1] - a[0]*b[0],
             - a[2]*b[3],
               a[1]*b[3],
             - a[0]*b[3],
               a[2]*b[1] - a[1]*b[2],
             - a[2]*b[0] + a[0]*b[2],
               a[1]*b[0] - a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[2]*b[2] - a[1]*b[1] - a[0]*b[0],
             - a[2]*b[3],
               a[1]*b[3],
             - a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_2<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[6] - a[1]*b[5] - a[0]*b[4],
               a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1],
               a[2]*b[5] - a[1]*b[6] + a[0]*b[0],
             - a[2]*b[4] + a[1]*b[0] + a[0]*b[6],
               a[2]*b[0] + a[1]*b[4] - a[0]*b[5],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[2]*b[6] - a[1]*b[5] - a[0]*b[4],
               a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[2]*b[4] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] + a[1]*b[4] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1] - a[0]*b[4],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[2]*b[4] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] + a[1]*b[4] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1] - a[0]*b[4],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[2]*b[3] - a[1]*b[2] - a[0]*b[1],
             - a[2]*b[4],
               a[1]*b[4],
             - a[0]*b[4],
               a[2]*b[2] - a[1]*b[3] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0] + a[0]*b[3],
               a[2]*b[0] + a[1]*b[1] - a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[2]*b[3] - a[1]*b[2] - a[0]*b[1],
             - a[2]*b[4],
               a[1]*b[4],
             - a[0]*b[4],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[5] - a[1]*b[4] - a[0]*b[3],
             - a[2]*b[6] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] + a[1]*b[6] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0] - a[0]*b[6],
               a[2]*b[4] - a[1]*b[5],
             - a[2]*b[3] + a[0]*b[5],
               a[1]*b[3] - a[0]*b[4],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[2]*b[5] - a[1]*b[4] - a[0]*b[3],
             - a[2]*b[6] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] + a[1]*b[6] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0] - a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_2<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[6] - a[1]*b[5] - a[0]*b[4],
             - a[2]*b[7] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] + a[1]*b[7] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1] - a[0]*b[7],
               a[2]*b[5] - a[1]*b[6] + a[0]*b[0],
             - a[2]*b[4] + a[1]*b[0] + a[0]*b[6],
               a[2]*b[0] + a[1]*b[4] - a[0]*b[5],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[2]*b[6] - a[1]*b[5] - a[0]*b[4],
             - a[2]*b[7] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] + a[1]*b[7] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1] - a[0]*b[7],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_3<T> (
    [T;1]
);

impl<T> MV3_3<T> {
    pub fn new(a0: T) -> Self {
        MV3_3 (
            [a0]
        )
    }
}

impl<T> MV3_3<T> where T: One {
    pub fn unit() -> Self {
        MV3_3 (
            [T::one()]
        )
    }
}

impl<T> VectorSpace for MV3_3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_3(
            [-self.0[0]]
        )
    }
}

impl<T> MV3_3<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_3<T> {
        let mut z = MV3_0_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_3 (
            [a[0] + b[0]]
        )
    }
}

impl<T> Sub for MV3_3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_3 (
            [a[0] - b[0]]
        )
    }
}

impl<T> Neg for MV3_3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_3 (
            [-a[0]]
        )
    }
}

impl<T> Mul<T> for MV3_3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_3 (
            [a[0] * other]
        )
    }
}

impl<T> Div<T> for MV3_3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_3 (
            [a[0] / other]
        )
    }
}

impl<T> Rem<T> for MV3_3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_3 (
            [a[0] % other]
        )
    }
}

impl<T> AddAssign for MV3_3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
    }
}

impl<T> SubAssign for MV3_3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
    }
}

impl<T> MulAssign<T> for MV3_3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
    }
}

impl<T> DivAssign<T> for MV3_3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
    }
}

impl<T> Zero for MV3_3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_3 ([ T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        true
    }
}

impl<T> From<MV3_3<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_1<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [- a[0]*b[2],
               a[0]*b[1],
             - a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [- a[0]*b[2],
               a[0]*b[1],
             - a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_0<T> {
        let a = self.0;
        let b = other.0;
        MV3_0(
            [- a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_0<T> {
        let a = self.0;
        let b = other.0;
        MV3_0(
            [- a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_3<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [- a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [- a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[0]*b[5],
               a[0]*b[4],
             - a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[0]*b[5],
               a[0]*b[4],
             - a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_1_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_3<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_1_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_3<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
             - a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
             - a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[0]*b[6],
               a[0]*b[5],
             - a[0]*b[4],
               a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[0]*b[6],
               a[0]*b[5],
             - a[0]*b[4],
               a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_3<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [- a[0]*b[4],
               a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [- a[0]*b[4],
               a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_3<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [- a[0]*b[4],
             - a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [- a[0]*b[4],
             - a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_3<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[0]*b[6],
             - a[0]*b[5],
               a[0]*b[4],
             - a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[0]*b[6],
             - a[0]*b[5],
               a[0]*b[4],
             - a[0]*b[3],
               a[0]*b[2],
             - a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_1_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_3<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[0]]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[0]*b[7],
             - a[0]*b[6],
               a[0]*b[5],
             - a[0]*b[4],
               a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[0]*b[7],
             - a[0]*b[6],
               a[0]*b[5],
             - a[0]*b[4],
               a[0]*b[3],
             - a[0]*b[2],
               a[0]*b[1],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_0_1<T> (
    [T;4]
);

impl<T> VectorSpace for MV3_0_1<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_0_1<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_0_1<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_0_1<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_0_1(
            [ self.0[0],
              self.0[1],
              self.0[2],
              self.0[3]]
        )
    }
}

impl<T> MV3_0_1<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_0_1<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_1 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3]]
        )
    }
}

impl<T> Sub for MV3_0_1<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_1 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3]]
        )
    }
}

impl<T> Neg for MV3_0_1<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_0_1 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3]]
        )
    }
}

impl<T> Mul<T> for MV3_0_1<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other]
        )
    }
}

impl<T> Div<T> for MV3_0_1<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other]
        )
    }
}

impl<T> Rem<T> for MV3_0_1<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other]
        )
    }
}

impl<T> AddAssign for MV3_0_1<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
    }
}

impl<T> SubAssign for MV3_0_1<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
    }
}

impl<T> MulAssign<T> for MV3_0_1<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
    }
}

impl<T> DivAssign<T> for MV3_0_1<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
    }
}

impl<T> Zero for MV3_0_1<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_0_1 ([ T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_0_1<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        true
    }
}

impl<T> From<T> for MV3_0_1<T> where T: Zero {
    fn from(a: T) -> Self {
        MV3_0_1 (
            [a,T::zero(),T::zero(),T::zero()]
        )
    }
}

impl<T> From<MV3_0_1<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_1(
            [a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[3]*b[1] - a[2]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[2]*b[2] + a[1]*b[1],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[3]*b[1] - a[2]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[2]*b[2] + a[1]*b[1],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_3(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2],
             - a[3]*b[1] + a[1]*b[3],
             - a[3]*b[2] + a[2]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2],
             - a[3]*b[1] + a[1]*b[3],
             - a[3]*b[2] + a[2]*b[3]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
             - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
               a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
             - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
             - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
               a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [a[0],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[3]*b[1],
             - a[2]*b[1],
               a[1]*b[1],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[3]*b[1],
             - a[2]*b[1],
               a[1]*b[1],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [a[1],
             a[2],
             a[3],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
             - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
               a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[3]*b[3] - a[2]*b[4] + a[1]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
             - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
               a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[3]*b[3] - a[2]*b[4] + a[1]*b[5]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [a[1],
             a[2],
             a[3],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[3] - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] - a[2]*b[3] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2] + a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[3],
             - a[2]*b[3],
               a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[3]*b[1] - a[2]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[2]*b[2] + a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[2]*b[3] + a[0]*b[1],
               a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[3]*b[1] - a[2]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[2]*b[2] + a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[2]*b[3] + a[0]*b[1],
               a[1]*b[3] + a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [a[0],
             a[1],
             a[2],
             a[3],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
             - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[3]*b[4] - a[2]*b[5] + a[1]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
             - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[3]*b[4] - a[2]*b[5] + a[1]*b[6]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [a[0],
             a[1],
             a[2],
             a[3],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[3]*b[4] - a[2]*b[1] + a[1]*b[2],
             - a[3]*b[1] - a[2]*b[4] + a[1]*b[3],
             - a[3]*b[2] + a[2]*b[3] + a[1]*b[4],
               a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[3]*b[4],
             - a[2]*b[4],
               a[1]*b[4],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2],
             - a[3]*b[1] + a[1]*b[3],
             - a[3]*b[2] + a[2]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
             - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
               a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[3]*b[4] + a[0]*b[1],
             - a[2]*b[4] + a[0]*b[2],
               a[1]*b[4] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
             - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
               a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[3]*b[4] + a[0]*b[1],
             - a[2]*b[4] + a[0]*b[2],
               a[1]*b[4] + a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [a[1],
             a[2],
             a[3],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
             - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
               a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[3]*b[6] - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] - a[2]*b[6] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[3] - a[2]*b[4] + a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
             - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
               a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[3]*b[6] + a[0]*b[3],
             - a[2]*b[6] + a[0]*b[4],
               a[1]*b[6] + a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[3]*b[3] - a[2]*b[4] + a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_0_1<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [a[0],
             a[1],
             a[2],
             a[3],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[3]*b[7] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] - a[2]*b[7] + a[1]*b[3] + a[0]*b[5],
             - a[3]*b[2] + a[2]*b[3] + a[1]*b[7] + a[0]*b[6],
               a[3]*b[4] - a[2]*b[5] + a[1]*b[6] + a[0]*b[7]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[3]*b[7] + a[0]*b[4],
             - a[2]*b[7] + a[0]*b[5],
               a[1]*b[7] + a[0]*b[6],
               a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_0_1<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
             - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[3]*b[4] - a[2]*b[5] + a[1]*b[6] + a[0]*b[7]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_0_2<T> (
    [T;4]
);

impl<T> VectorSpace for MV3_0_2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_0_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_0_2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_0_2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_0_2(
            [ self.0[0],
             -self.0[1],
             -self.0[2],
             -self.0[3]]
        )
    }
}

impl<T> MV3_0_2<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_2<T> {
        let mut z = MV3_0_2::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_2::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_0_2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_2 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3]]
        )
    }
}

impl<T> Sub for MV3_0_2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_2 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3]]
        )
    }
}

impl<T> Neg for MV3_0_2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_0_2 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3]]
        )
    }
}

impl<T> Mul<T> for MV3_0_2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_2 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other]
        )
    }
}

impl<T> Div<T> for MV3_0_2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_2 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other]
        )
    }
}

impl<T> Rem<T> for MV3_0_2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_2 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other]
        )
    }
}

impl<T> AddAssign for MV3_0_2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
    }
}

impl<T> SubAssign for MV3_0_2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
    }
}

impl<T> MulAssign<T> for MV3_0_2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
    }
}

impl<T> DivAssign<T> for MV3_0_2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
    }
}

impl<T> Zero for MV3_0_2<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_0_2 ([ T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_0_2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        true
    }
}

impl<T> One for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV3_0_2 ([T::one(),T::zero(),T::zero(),T::zero()]) }
}

impl<T> From<T> for MV3_0_2<T> where T: Zero {
    fn from(a: T) -> Self {
        MV3_0_2 (
            [a,T::zero(),T::zero(),T::zero()]
        )
    }
}

impl<T> From<MV3_0_2<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] + a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_2(
            [a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_3(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
               a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [a[0],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[3]*b[1],
               a[2]*b[1],
             - a[1]*b[1],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[3]*b[1],
               a[2]*b[1],
             - a[1]*b[1],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
               a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[3]*b[4] - a[2]*b[5] + a[0]*b[3],
             - a[3]*b[3] + a[1]*b[5] + a[0]*b[4],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
               a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[2]*b[3] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] - a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[2]*b[3] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] - a[1]*b[3] + a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [a[1],
             a[2],
             a[3],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
             - a[3]*b[3],
               a[2]*b[3],
             - a[1]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
             - a[3]*b[3],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[3]*b[5] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[3]*b[4] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[0] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[0]*b[5],
               a[3]*b[0] + a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[0]*b[5],
               a[3]*b[0] + a[0]*b[6],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] + a[2]*b[4] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[4] + a[0]*b[3],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] + a[2]*b[4] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[4] + a[0]*b[3],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [a[0],
             a[1],
             a[2],
             a[3],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[3]*b[4],
               a[2]*b[4],
             - a[1]*b[4],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[3]*b[4],
               a[2]*b[4],
             - a[1]*b[4],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[1],
             a[2],
             a[3],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
             - a[3]*b[6] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[2]*b[6] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] - a[1]*b[6] + a[0]*b[2],
               a[3]*b[4] - a[2]*b[5] + a[0]*b[3],
             - a[3]*b[3] + a[1]*b[5] + a[0]*b[4],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
             - a[3]*b[6] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[2]*b[6] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] - a[1]*b[6] + a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_0_2<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             a[1],
             a[2],
             a[3],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[3]*b[7] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] + a[2]*b[7] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[7] + a[0]*b[3],
               a[3]*b[5] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[3]*b[4] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[0] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[7]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[3]*b[7] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] + a[2]*b[7] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[7] + a[0]*b[3],
               a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[0]*b[5],
               a[3]*b[0] + a[0]*b[6],
               a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_0_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[0]*b[5],
               a[3]*b[0] + a[0]*b[6],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[7]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_0_3<T> (
    [T;2]
);

impl<T> VectorSpace for MV3_0_3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_0_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_0_3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_0_3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_0_3(
            [ self.0[0],
             -self.0[1]]
        )
    }
}

impl<T> MV3_0_3<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_3<T> {
        let mut z = MV3_0_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_0_3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_3 (
            [a[0] + b[0],
             a[1] + b[1]]
        )
    }
}

impl<T> Sub for MV3_0_3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_3 (
            [a[0] - b[0],
             a[1] - b[1]]
        )
    }
}

impl<T> Neg for MV3_0_3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_0_3 (
            [-a[0],
             -a[1]]
        )
    }
}

impl<T> Mul<T> for MV3_0_3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_3 (
            [a[0] * other,
             a[1] * other]
        )
    }
}

impl<T> Div<T> for MV3_0_3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_3 (
            [a[0] / other,
             a[1] / other]
        )
    }
}

impl<T> Rem<T> for MV3_0_3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_3 (
            [a[0] % other,
             a[1] % other]
        )
    }
}

impl<T> AddAssign for MV3_0_3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
    }
}

impl<T> SubAssign for MV3_0_3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
    }
}

impl<T> MulAssign<T> for MV3_0_3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
    }
}

impl<T> DivAssign<T> for MV3_0_3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
    }
}

impl<T> Zero for MV3_0_3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_0_3 ([ T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_0_3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        true
    }
}

impl<T> One for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV3_0_3 ([T::one(),T::zero()]) }
}

impl<T> From<T> for MV3_0_3<T> where T: Zero {
    fn from(a: T) -> Self {
        MV3_0_3 (
            [a,T::zero()]
        )
    }
}

impl<T> From<MV3_0_3<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [  a[0]*b[0],
               a[1]*b[0]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[1]*b[2],
             - a[1]*b[1],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[1]*b[2],
             - a[1]*b[1],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[1]*b[2],
               a[1]*b[1],
             - a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[1]*b[2],
               a[1]*b[1],
             - a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_3(
            [a[1]]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [- a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [- a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[3],
             - a[1]*b[2],
               a[1]*b[1],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[3],
             - a[1]*b[2],
               a[1]*b[1],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[1]*b[3],
               a[1]*b[2],
             - a[1]*b[1],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[1]*b[3],
               a[1]*b[2],
             - a[1]*b[1],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0]]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [- a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [- a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_0_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[1]*b[5] + a[0]*b[0],
               a[1]*b[4] + a[0]*b[1],
             - a[1]*b[3] + a[0]*b[2],
               a[1]*b[2] + a[0]*b[3],
             - a[1]*b[1] + a[0]*b[4],
               a[1]*b[0] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[1]*b[5] + a[0]*b[0],
               a[1]*b[4] + a[0]*b[1],
             - a[1]*b[3] + a[0]*b[2],
               a[1]*b[2] + a[0]*b[3],
             - a[1]*b[1] + a[0]*b[4],
               a[1]*b[0] + a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[1]]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[3],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[1]*b[2],
             - a[1]*b[1],
               a[1]*b[0],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[3],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[1]*b[2],
             - a[1]*b[1],
               a[1]*b[0],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[1]]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[3],
             - a[1]*b[2],
               a[1]*b[1],
             - a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[3],
             - a[1]*b[2],
               a[1]*b[1],
             - a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[1]*b[6] + a[0]*b[1],
               a[1]*b[5] + a[0]*b[2],
             - a[1]*b[4] + a[0]*b[3],
               a[1]*b[3] + a[0]*b[4],
             - a[1]*b[2] + a[0]*b[5],
               a[1]*b[1] + a[0]*b[6],
               a[1]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[1]*b[6] + a[0]*b[1],
               a[1]*b[5] + a[0]*b[2],
             - a[1]*b[4] + a[0]*b[3],
               a[1]*b[3] + a[0]*b[4],
             - a[1]*b[2] + a[0]*b[5],
               a[1]*b[1] + a[0]*b[6],
               a[1]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6],
               a[1]*b[0]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             a[1]]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[4] + a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[3],
             - a[1]*b[2],
               a[1]*b[1],
               a[1]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[4] + a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[3],
             - a[1]*b[2],
               a[1]*b[1],
               a[1]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             a[1]]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[4] + a[0]*b[0],
             - a[1]*b[3],
               a[1]*b[2],
             - a[1]*b[1],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[4] + a[0]*b[0],
             - a[1]*b[3],
               a[1]*b[2],
             - a[1]*b[1],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[1]]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[6],
             - a[1]*b[5] + a[0]*b[0],
               a[1]*b[4] + a[0]*b[1],
             - a[1]*b[3] + a[0]*b[2],
               a[1]*b[2] + a[0]*b[3],
             - a[1]*b[1] + a[0]*b[4],
               a[1]*b[0] + a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[6],
             - a[1]*b[5] + a[0]*b[0],
               a[1]*b[4] + a[0]*b[1],
             - a[1]*b[3] + a[0]*b[2],
               a[1]*b[2] + a[0]*b[3],
             - a[1]*b[1] + a[0]*b[4],
               a[1]*b[0] + a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> From<MV3_0_3<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[1]]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[7] + a[0]*b[0],
             - a[1]*b[6] + a[0]*b[1],
               a[1]*b[5] + a[0]*b[2],
             - a[1]*b[4] + a[0]*b[3],
               a[1]*b[3] + a[0]*b[4],
             - a[1]*b[2] + a[0]*b[5],
               a[1]*b[1] + a[0]*b[6],
               a[1]*b[0] + a[0]*b[7]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[1]*b[7] + a[0]*b[0],
             - a[1]*b[6] + a[0]*b[1],
               a[1]*b[5] + a[0]*b[2],
             - a[1]*b[4] + a[0]*b[3],
               a[1]*b[3] + a[0]*b[4],
             - a[1]*b[2] + a[0]*b[5],
               a[1]*b[1] + a[0]*b[6],
               a[1]*b[0] + a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_0_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[0]*b[6],
               a[1]*b[0] + a[0]*b[7]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_1_2<T> (
    [T;6]
);

impl<T> VectorSpace for MV3_1_2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_1_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_1_2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_1_2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_1_2(
            [ self.0[0],
              self.0[1],
              self.0[2],
             -self.0[3],
             -self.0[4],
             -self.0[5]]
        )
    }
}

impl<T> MV3_1_2<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_1_2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_1_2 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3],
             a[4] + b[4],
             a[5] + b[5]]
        )
    }
}

impl<T> Sub for MV3_1_2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_1_2 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3],
             a[4] - b[4],
             a[5] - b[5]]
        )
    }
}

impl<T> Neg for MV3_1_2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_1_2 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3],
             -a[4],
             -a[5]]
        )
    }
}

impl<T> Mul<T> for MV3_1_2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_2 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other,
             a[4] * other,
             a[5] * other]
        )
    }
}

impl<T> Div<T> for MV3_1_2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_2 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other,
             a[4] / other,
             a[5] / other]
        )
    }
}

impl<T> Rem<T> for MV3_1_2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_2 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other,
             a[4] % other,
             a[5] % other]
        )
    }
}

impl<T> AddAssign for MV3_1_2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
        a[4] += b[4];
        a[5] += b[5];
    }
}

impl<T> SubAssign for MV3_1_2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
        a[4] -= b[4];
        a[5] -= b[5];
    }
}

impl<T> MulAssign<T> for MV3_1_2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
        a[4] *= other;
        a[5] *= other;
    }
}

impl<T> DivAssign<T> for MV3_1_2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
        a[4] /= other;
        a[5] /= other;
    }
}

impl<T> Zero for MV3_1_2<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_1_2 ([ T::zero(), T::zero(), T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        self.0[4].is_zero()&&
        self.0[5].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_1_2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        self.0[4]==other.0[4]&&
        self.0[5]==other.0[5]&&
        true
    }
}

impl<T> From<MV3_1_2<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_1(
            [a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[2] + a[3]*b[1],
               a[5]*b[2] - a[3]*b[0],
             - a[5]*b[1] - a[4]*b[0],
             - a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[2] + a[3]*b[1],
               a[5]*b[2] - a[3]*b[0],
             - a[5]*b[1] - a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_2(
            [a[3],
             a[4],
             a[5]]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[2] - a[4]*b[1] - a[3]*b[0],
             - a[2]*b[1] - a[1]*b[0],
             - a[2]*b[2] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1],
               a[5]*b[1] - a[4]*b[2],
             - a[5]*b[0] + a[3]*b[2],
               a[4]*b[0] - a[3]*b[1],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[5]*b[2] - a[4]*b[1] - a[3]*b[0],
             - a[2]*b[1] - a[1]*b[0],
             - a[2]*b[2] + a[0]*b[0],
               a[1]*b[2] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_3(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[5]*b[0],
               a[4]*b[0],
             - a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[5]*b[0],
               a[4]*b[0],
             - a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[3] + a[3]*b[2] + a[0]*b[0],
               a[5]*b[3] - a[3]*b[1] + a[1]*b[0],
             - a[5]*b[2] - a[4]*b[1] + a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[5]*b[1] - a[4]*b[2] + a[3]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[3] + a[3]*b[2] + a[0]*b[0],
               a[5]*b[3] - a[3]*b[1] + a[1]*b[0],
             - a[5]*b[2] - a[4]*b[1] + a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[5]*b[1] - a[4]*b[2] + a[3]*b[3]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [T::zero(),
             a[3],
             a[4],
             a[5]]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[3] - a[4]*b[2] - a[3]*b[1],
             - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[5]*b[2] - a[4]*b[3] + a[3]*b[0],
             - a[5]*b[1] + a[4]*b[0] + a[3]*b[3],
               a[5]*b[0] + a[4]*b[1] - a[3]*b[2],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[5]*b[3] - a[4]*b[2] - a[3]*b[1],
             - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[5]*b[1] + a[0]*b[0],
               a[4]*b[1] + a[1]*b[0],
             - a[3]*b[1] + a[2]*b[0],
               a[3]*b[0] + a[2]*b[1],
               a[4]*b[0] - a[1]*b[1],
               a[5]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[5]*b[1] + a[0]*b[0],
               a[4]*b[1] + a[1]*b[0],
             - a[3]*b[1] + a[2]*b[0],
               a[3]*b[0] + a[2]*b[1],
               a[4]*b[0] - a[1]*b[1],
               a[5]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0]]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[5] - a[4]*b[4] - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[2] + a[3]*b[1] - a[2]*b[4] - a[1]*b[3],
               a[5]*b[2] - a[3]*b[0] - a[2]*b[5] + a[0]*b[3],
             - a[5]*b[1] - a[4]*b[0] + a[1]*b[5] + a[0]*b[4],
               a[5]*b[4] - a[4]*b[5] - a[1]*b[0] + a[0]*b[1],
             - a[5]*b[3] + a[3]*b[5] - a[2]*b[0] + a[0]*b[2],
               a[4]*b[3] - a[3]*b[4] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[5]*b[5] - a[4]*b[4] - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[2] + a[3]*b[1] - a[2]*b[4] - a[1]*b[3],
               a[5]*b[2] - a[3]*b[0] - a[2]*b[5] + a[0]*b[3],
             - a[5]*b[1] - a[4]*b[0] + a[1]*b[5] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[5]*b[3] + a[4]*b[2] + a[3]*b[1],
               a[5]*b[2] + a[4]*b[3] - a[3]*b[0],
             - a[5]*b[1] - a[4]*b[0] - a[3]*b[3],
               a[2]*b[3] - a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] - a[1]*b[3] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[3],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[5]*b[3] + a[4]*b[2] + a[3]*b[1],
               a[5]*b[2] + a[4]*b[3] - a[3]*b[0],
             - a[5]*b[1] - a[4]*b[0] - a[3]*b[3],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [a[3],
             a[4],
             a[5],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[2] - a[4]*b[1] - a[3]*b[0],
             - a[5]*b[3] - a[2]*b[1] - a[1]*b[0],
               a[4]*b[3] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[5]*b[1] - a[4]*b[2] + a[2]*b[3],
             - a[5]*b[0] + a[3]*b[2] - a[1]*b[3],
               a[4]*b[0] - a[3]*b[1] + a[0]*b[3],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[5]*b[2] - a[4]*b[1] - a[3]*b[0],
             - a[5]*b[3] - a[2]*b[1] - a[1]*b[0],
               a[4]*b[3] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             a[3],
             a[4],
             a[5]]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[6] - a[4]*b[5] - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[3] + a[3]*b[2] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[5]*b[3] - a[3]*b[1] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[5]*b[2] - a[4]*b[1] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[5]*b[5] - a[4]*b[6] + a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
             - a[5]*b[4] + a[4]*b[0] + a[3]*b[6] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] + a[4]*b[4] - a[3]*b[5] - a[2]*b[2] + a[1]*b[3],
               a[5]*b[1] - a[4]*b[2] + a[3]*b[3] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[5]*b[6] - a[4]*b[5] - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[3] + a[3]*b[2] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[5]*b[3] - a[3]*b[1] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[5]*b[2] - a[4]*b[1] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[5]*b[1] - a[4]*b[2] + a[3]*b[3] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[5]*b[4] + a[4]*b[3] + a[3]*b[2] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[4] - a[3]*b[1] + a[1]*b[0],
             - a[5]*b[2] - a[4]*b[1] - a[3]*b[4] + a[2]*b[0],
               a[3]*b[0] + a[2]*b[4] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] - a[1]*b[4] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4],
               a[5]*b[1] - a[4]*b[2] + a[3]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[5]*b[4] + a[4]*b[3] + a[3]*b[2] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[4] - a[3]*b[1] + a[1]*b[0],
             - a[5]*b[2] - a[4]*b[1] - a[3]*b[4] + a[2]*b[0],
               a[3]*b[0] + a[2]*b[4],
               a[4]*b[0] - a[1]*b[4],
               a[5]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[5]*b[1] - a[4]*b[2] + a[3]*b[3]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [T::zero(),
             a[3],
             a[4],
             a[5],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[3] - a[4]*b[2] - a[3]*b[1],
             - a[5]*b[4] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[4]*b[4] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[4] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[5]*b[2] - a[4]*b[3] + a[3]*b[0] + a[2]*b[4],
             - a[5]*b[1] + a[4]*b[0] + a[3]*b[3] - a[1]*b[4],
               a[5]*b[0] + a[4]*b[1] - a[3]*b[2] + a[0]*b[4],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[5]*b[3] - a[4]*b[2] - a[3]*b[1],
             - a[5]*b[4] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[4]*b[4] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[4] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[4],
               a[4]*b[0] - a[1]*b[4],
               a[5]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [a[0],
             a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[5] - a[4]*b[4] - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[5]*b[6] + a[4]*b[2] + a[3]*b[1] - a[2]*b[4] - a[1]*b[3],
               a[5]*b[2] + a[4]*b[6] - a[3]*b[0] - a[2]*b[5] + a[0]*b[3],
             - a[5]*b[1] - a[4]*b[0] - a[3]*b[6] + a[1]*b[5] + a[0]*b[4],
               a[5]*b[4] - a[4]*b[5] + a[2]*b[6] - a[1]*b[0] + a[0]*b[1],
             - a[5]*b[3] + a[3]*b[5] - a[2]*b[0] - a[1]*b[6] + a[0]*b[2],
               a[4]*b[3] - a[3]*b[4] - a[2]*b[1] + a[1]*b[2] + a[0]*b[6],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[5]*b[5] - a[4]*b[4] - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[5]*b[6] + a[4]*b[2] + a[3]*b[1] - a[2]*b[4] - a[1]*b[3],
               a[5]*b[2] + a[4]*b[6] - a[3]*b[0] - a[2]*b[5] + a[0]*b[3],
             - a[5]*b[1] - a[4]*b[0] - a[3]*b[6] + a[1]*b[5] + a[0]*b[4],
               a[2]*b[6],
             - a[1]*b[6],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> From<MV3_1_2<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[6] - a[4]*b[5] - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[5]*b[7] + a[4]*b[3] + a[3]*b[2] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[7] - a[3]*b[1] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[5]*b[2] - a[4]*b[1] - a[3]*b[7] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[5]*b[5] - a[4]*b[6] + a[3]*b[0] + a[2]*b[7] - a[1]*b[1] + a[0]*b[2],
             - a[5]*b[4] + a[4]*b[0] + a[3]*b[6] - a[2]*b[1] - a[1]*b[7] + a[0]*b[3],
               a[5]*b[0] + a[4]*b[4] - a[3]*b[5] - a[2]*b[2] + a[1]*b[3] + a[0]*b[7],
               a[5]*b[1] - a[4]*b[2] + a[3]*b[3] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[5]*b[6] - a[4]*b[5] - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[5]*b[7] + a[4]*b[3] + a[3]*b[2] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[7] - a[3]*b[1] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[5]*b[2] - a[4]*b[1] - a[3]*b[7] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[0] + a[2]*b[7],
               a[4]*b[0] - a[1]*b[7],
               a[5]*b[0] + a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[5]*b[1] - a[4]*b[2] + a[3]*b[3] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_1_3<T> (
    [T;4]
);

impl<T> VectorSpace for MV3_1_3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_1_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_1_3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_1_3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_1_3(
            [ self.0[0],
              self.0[1],
              self.0[2],
             -self.0[3]]
        )
    }
}

impl<T> MV3_1_3<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_1_3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_1_3 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3]]
        )
    }
}

impl<T> Sub for MV3_1_3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_1_3 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3]]
        )
    }
}

impl<T> Neg for MV3_1_3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_1_3 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3]]
        )
    }
}

impl<T> Mul<T> for MV3_1_3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_3 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other]
        )
    }
}

impl<T> Div<T> for MV3_1_3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_3 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other]
        )
    }
}

impl<T> Rem<T> for MV3_1_3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_3 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other]
        )
    }
}

impl<T> AddAssign for MV3_1_3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
    }
}

impl<T> SubAssign for MV3_1_3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
    }
}

impl<T> MulAssign<T> for MV3_1_3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
    }
}

impl<T> DivAssign<T> for MV3_1_3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
    }
}

impl<T> Zero for MV3_1_3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_1_3 ([ T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_1_3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        true
    }
}

impl<T> From<MV3_1_3<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2],
             - a[3]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_1(
            [- a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_3(
            [a[3]]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[3],
             - a[3]*b[2],
               a[3]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [T::zero(),
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1],
             - a[1]*b[1],
               a[0]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[2]*b[1],
             - a[1]*b[1],
               a[0]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [a[0],
             a[1],
             a[2],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
               a[3]*b[4] - a[2]*b[5] + a[0]*b[3],
             - a[3]*b[3] + a[1]*b[5] + a[0]*b[4],
               a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
               a[3]*b[4] - a[2]*b[5] + a[0]*b[3],
             - a[3]*b[3] + a[1]*b[5] + a[0]*b[4],
               a[3]*b[2],
             - a[3]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[2]*b[3] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] - a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2(
            [- a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[2]*b[3],
             - a[3]*b[1] - a[1]*b[3],
               a[3]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[3]]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[3],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[3],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[3],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[3]*b[5] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[3]*b[4] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3],
               a[3]*b[0] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[3]*b[5] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[3]*b[4] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[3],
             - a[3]*b[2],
               a[3]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3],
               a[3]*b[0] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[3] + a[2]*b[4] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[4] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[3] + a[2]*b[4],
             - a[3]*b[2] - a[1]*b[4],
               a[3]*b[1] + a[0]*b[4],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[4],
             - a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[4],
             - a[1]*b[4],
               a[0]*b[4],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[4],
             - a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[4],
             - a[1]*b[4],
               a[0]*b[4],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [a[0],
             a[1],
             a[2],
             T::zero(),
             T::zero(),
             T::zero(),
             a[3]]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[6] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
               a[3]*b[4] - a[2]*b[5] + a[0]*b[3],
             - a[3]*b[3] + a[1]*b[5] + a[0]*b[4],
               a[3]*b[2] + a[2]*b[6] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] - a[1]*b[6] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[6],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[6] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
               a[3]*b[4] - a[2]*b[5] + a[0]*b[3],
             - a[3]*b[3] + a[1]*b[5] + a[0]*b[4],
               a[3]*b[2] + a[2]*b[6],
             - a[3]*b[1] - a[1]*b[6],
               a[3]*b[0] + a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> From<MV3_1_3<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             T::zero(),
             T::zero(),
             T::zero(),
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[7] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[3]*b[5] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[3]*b[4] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[3] + a[2]*b[7] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[7] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[7],
               a[3]*b[0] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[7] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[3]*b[5] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[3]*b[4] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[3] + a[2]*b[7],
             - a[3]*b[2] - a[1]*b[7],
               a[3]*b[1] + a[0]*b[7],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
             - a[1]*b[1] + a[0]*b[2],
             - a[2]*b[1] + a[0]*b[3],
             - a[2]*b[2] + a[1]*b[3],
               a[3]*b[0] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_2_3<T> (
    [T;4]
);

impl<T> VectorSpace for MV3_2_3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_2_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_2_3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_2_3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_2_3(
            [-self.0[0],
             -self.0[1],
             -self.0[2],
             -self.0[3]]
        )
    }
}

impl<T> MV3_2_3<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_2_3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_2_3 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3]]
        )
    }
}

impl<T> Sub for MV3_2_3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_2_3 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3]]
        )
    }
}

impl<T> Neg for MV3_2_3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_2_3 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3]]
        )
    }
}

impl<T> Mul<T> for MV3_2_3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_2_3 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other]
        )
    }
}

impl<T> Div<T> for MV3_2_3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_2_3 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other]
        )
    }
}

impl<T> Rem<T> for MV3_2_3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_2_3 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other]
        )
    }
}

impl<T> AddAssign for MV3_2_3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
    }
}

impl<T> SubAssign for MV3_2_3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
    }
}

impl<T> MulAssign<T> for MV3_2_3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
    }
}

impl<T> DivAssign<T> for MV3_2_3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
    }
}

impl<T> Zero for MV3_2_3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_2_3 ([ T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_2_3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        true
    }
}

impl<T> From<MV3_2_3<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0],
               a[3]*b[2],
             - a[3]*b[1],
               a[3]*b[0],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0],
               a[3]*b[2],
             - a[3]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[2]*b[2] - a[1]*b[1] - a[0]*b[0],
             - a[3]*b[2],
               a[3]*b[1],
             - a[3]*b[0],
               a[2]*b[1] - a[1]*b[2],
             - a[2]*b[0] + a[0]*b[2],
               a[1]*b[0] - a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[2]*b[2] - a[1]*b[1] - a[0]*b[0],
             - a[3]*b[2],
               a[3]*b[1],
             - a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_2<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_3(
            [a[3]]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
             - a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
             - a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[3]*b[1] + a[2]*b[0],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[3]*b[1] + a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[3] - a[1]*b[2] - a[0]*b[1],
             - a[3]*b[3],
               a[3]*b[2],
             - a[3]*b[1],
               a[2]*b[2] - a[1]*b[3] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0] + a[0]*b[3],
               a[2]*b[0] + a[1]*b[1] - a[0]*b[2],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[3] - a[1]*b[2] - a[0]*b[1],
             - a[3]*b[3],
               a[3]*b[2],
             - a[3]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [T::zero(),
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[1],
             - a[2]*b[1],
               a[1]*b[1],
             - a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[1],
             - a[2]*b[1],
               a[1]*b[1],
             - a[0]*b[1],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[5] - a[1]*b[4] - a[0]*b[3],
             - a[3]*b[5] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[4] + a[2]*b[2] - a[0]*b[0],
             - a[3]*b[3] - a[2]*b[1] - a[1]*b[0],
               a[3]*b[2] + a[2]*b[4] - a[1]*b[5],
             - a[3]*b[1] - a[2]*b[3] + a[0]*b[5],
               a[3]*b[0] + a[1]*b[3] - a[0]*b[4],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[2]*b[5] - a[1]*b[4] - a[0]*b[3],
             - a[3]*b[5] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[4] + a[2]*b[2] - a[0]*b[0],
             - a[3]*b[3] - a[2]*b[1] - a[1]*b[0],
               a[3]*b[2],
             - a[3]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[3]]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[3],
             - a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] + a[1]*b[3] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0] - a[0]*b[3],
               a[3]*b[2],
             - a[3]*b[1],
               a[3]*b[0],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[3],
             - a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[2] + a[1]*b[3] - a[0]*b[0],
             - a[2]*b[1] - a[1]*b[0] - a[0]*b[3],
               a[3]*b[2],
             - a[3]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] - a[0]*b[0],
             - a[3]*b[2] - a[2]*b[3],
               a[3]*b[1] + a[1]*b[3],
             - a[3]*b[0] - a[0]*b[3],
               a[2]*b[1] - a[1]*b[2],
             - a[2]*b[0] + a[0]*b[2],
               a[1]*b[0] - a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] - a[0]*b[0],
             - a[3]*b[2] - a[2]*b[3],
               a[3]*b[1] + a[1]*b[3],
             - a[3]*b[0] - a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_2_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[6] - a[1]*b[5] - a[0]*b[4],
             - a[3]*b[6] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[5] + a[2]*b[3] - a[0]*b[1],
             - a[3]*b[4] - a[2]*b[2] - a[1]*b[1],
               a[3]*b[3] + a[2]*b[5] - a[1]*b[6] + a[0]*b[0],
             - a[3]*b[2] - a[2]*b[4] + a[1]*b[0] + a[0]*b[6],
               a[3]*b[1] + a[2]*b[0] + a[1]*b[4] - a[0]*b[5],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[2]*b[6] - a[1]*b[5] - a[0]*b[4],
             - a[3]*b[6] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[5] + a[2]*b[3] - a[0]*b[1],
             - a[3]*b[4] - a[2]*b[2] - a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[3]*b[1] + a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[4],
             - a[2]*b[4] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] + a[1]*b[4] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1] - a[0]*b[4],
               a[3]*b[3] + a[0]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[3]*b[1] + a[2]*b[0],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[4],
             - a[2]*b[4] + a[1]*b[3] + a[0]*b[2],
               a[2]*b[3] + a[1]*b[4] - a[0]*b[1],
             - a[2]*b[2] - a[1]*b[1] - a[0]*b[4],
               a[3]*b[3] + a[0]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[3]*b[1] + a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[4] - a[2]*b[3] - a[1]*b[2] - a[0]*b[1],
             - a[3]*b[3] - a[2]*b[4],
               a[3]*b[2] + a[1]*b[4],
             - a[3]*b[1] - a[0]*b[4],
               a[2]*b[2] - a[1]*b[3] + a[0]*b[0],
             - a[2]*b[1] + a[1]*b[0] + a[0]*b[3],
               a[2]*b[0] + a[1]*b[1] - a[0]*b[2],
               a[3]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[4] - a[2]*b[3] - a[1]*b[2] - a[0]*b[1],
             - a[3]*b[3] - a[2]*b[4],
               a[3]*b[2] + a[1]*b[4],
             - a[3]*b[1] - a[0]*b[4],
               a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[0],
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[6] - a[2]*b[5] - a[1]*b[4] - a[0]*b[3],
             - a[3]*b[5] - a[2]*b[6] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[4] + a[2]*b[2] + a[1]*b[6] - a[0]*b[0],
             - a[3]*b[3] - a[2]*b[1] - a[1]*b[0] - a[0]*b[6],
               a[3]*b[2] + a[2]*b[4] - a[1]*b[5],
             - a[3]*b[1] - a[2]*b[3] + a[0]*b[5],
               a[3]*b[0] + a[1]*b[3] - a[0]*b[4],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[6] - a[2]*b[5] - a[1]*b[4] - a[0]*b[3],
             - a[3]*b[5] - a[2]*b[6] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[4] + a[2]*b[2] + a[1]*b[6] - a[0]*b[0],
             - a[3]*b[3] - a[2]*b[1] - a[1]*b[0] - a[0]*b[6],
               a[3]*b[2],
             - a[3]*b[1],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_2_3<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             T::zero(),
             a[0],
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[7] - a[2]*b[6] - a[1]*b[5] - a[0]*b[4],
             - a[3]*b[6] - a[2]*b[7] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[5] + a[2]*b[3] + a[1]*b[7] - a[0]*b[1],
             - a[3]*b[4] - a[2]*b[2] - a[1]*b[1] - a[0]*b[7],
               a[3]*b[3] + a[2]*b[5] - a[1]*b[6] + a[0]*b[0],
             - a[3]*b[2] - a[2]*b[4] + a[1]*b[0] + a[0]*b[6],
               a[3]*b[1] + a[2]*b[0] + a[1]*b[4] - a[0]*b[5],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[7] - a[2]*b[6] - a[1]*b[5] - a[0]*b[4],
             - a[3]*b[6] - a[2]*b[7] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[5] + a[2]*b[3] + a[1]*b[7] - a[0]*b[1],
             - a[3]*b[4] - a[2]*b[2] - a[1]*b[1] - a[0]*b[7],
               a[3]*b[3] + a[0]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[3]*b[1] + a[2]*b[0],
               a[3]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_0_1_2<T> (
    [T;7]
);

impl<T> VectorSpace for MV3_0_1_2<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_0_1_2<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_0_1_2<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_0_1_2(
            [ self.0[0],
              self.0[1],
              self.0[2],
              self.0[3],
             -self.0[4],
             -self.0[5],
             -self.0[6]]
        )
    }
}

impl<T> MV3_0_1_2<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_0_1_2<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3],
             a[4] + b[4],
             a[5] + b[5],
             a[6] + b[6]]
        )
    }
}

impl<T> Sub for MV3_0_1_2<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3],
             a[4] - b[4],
             a[5] - b[5],
             a[6] - b[6]]
        )
    }
}

impl<T> Neg for MV3_0_1_2<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_0_1_2 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3],
             -a[4],
             -a[5],
             -a[6]]
        )
    }
}

impl<T> Mul<T> for MV3_0_1_2<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_2 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other,
             a[4] * other,
             a[5] * other,
             a[6] * other]
        )
    }
}

impl<T> Div<T> for MV3_0_1_2<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_2 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other,
             a[4] / other,
             a[5] / other,
             a[6] / other]
        )
    }
}

impl<T> Rem<T> for MV3_0_1_2<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_2 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other,
             a[4] % other,
             a[5] % other,
             a[6] % other]
        )
    }
}

impl<T> AddAssign for MV3_0_1_2<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
        a[4] += b[4];
        a[5] += b[5];
        a[6] += b[6];
    }
}

impl<T> SubAssign for MV3_0_1_2<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
        a[4] -= b[4];
        a[5] -= b[5];
        a[6] -= b[6];
    }
}

impl<T> MulAssign<T> for MV3_0_1_2<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
        a[4] *= other;
        a[5] *= other;
        a[6] *= other;
    }
}

impl<T> DivAssign<T> for MV3_0_1_2<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
        a[4] /= other;
        a[5] /= other;
        a[6] /= other;
    }
}

impl<T> Zero for MV3_0_1_2<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_0_1_2 ([ T::zero(), T::zero(), T::zero(), T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        self.0[4].is_zero()&&
        self.0[5].is_zero()&&
        self.0[6].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_0_1_2<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        self.0[4]==other.0[4]&&
        self.0[5]==other.0[5]&&
        self.0[6]==other.0[6]&&
        true
    }
}

impl<T> From<T> for MV3_0_1_2<T> where T: Zero {
    fn from(a: T) -> Self {
        MV3_0_1_2 (
            [a,T::zero(),T::zero(),T::zero(),T::zero(),T::zero(),T::zero()]
        )
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_1(
            [a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[5]*b[2] + a[4]*b[1] + a[0]*b[0],
               a[6]*b[2] - a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] + a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[5]*b[2] + a[4]*b[1] + a[0]*b[0],
               a[6]*b[2] - a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] + a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_2(
            [a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[2] - a[5]*b[1] - a[4]*b[0],
             - a[3]*b[1] - a[2]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[2]*b[2] + a[1]*b[1],
               a[6]*b[1] - a[5]*b[2] + a[0]*b[0],
             - a[6]*b[0] + a[4]*b[2] + a[0]*b[1],
               a[5]*b[0] - a[4]*b[1] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[2] - a[5]*b[1] - a[4]*b[0],
             - a[3]*b[1] - a[2]*b[0],
             - a[3]*b[2] + a[1]*b[0],
               a[2]*b[2] + a[1]*b[1],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_3(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[6]*b[0],
               a[5]*b[0],
             - a[4]*b[0],
               a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[6]*b[0],
               a[5]*b[0],
             - a[4]*b[0],
               a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [a[0],
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[2] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] - a[4]*b[1] + a[2]*b[0] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] + a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3],
               a[6]*b[1] - a[5]*b[2] + a[4]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[2] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] - a[4]*b[1] + a[2]*b[0] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] + a[3]*b[0] + a[0]*b[3],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3],
               a[6]*b[1] - a[5]*b[2] + a[4]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [a[0],
             a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[3] - a[5]*b[2] - a[4]*b[1] + a[0]*b[0],
             - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
             - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
               a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[6]*b[2] - a[5]*b[3] + a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] + a[5]*b[0] + a[4]*b[3] + a[0]*b[2],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[3] - a[5]*b[2] - a[4]*b[1] + a[0]*b[0],
             - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
             - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
               a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[4]*b[0] + a[0]*b[1],
               a[5]*b[0] + a[0]*b[2],
               a[6]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[0]*b[1],
               a[5]*b[0] + a[0]*b[2],
               a[6]*b[0] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [a[0],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[6]*b[1] + a[1]*b[0],
               a[5]*b[1] + a[2]*b[0],
             - a[4]*b[1] + a[3]*b[0],
               a[4]*b[0] + a[3]*b[1],
               a[5]*b[0] - a[2]*b[1],
               a[6]*b[0] + a[1]*b[1],
               a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[6]*b[1] + a[1]*b[0],
               a[5]*b[1] + a[2]*b[0],
             - a[4]*b[1] + a[3]*b[0],
               a[4]*b[0] + a[3]*b[1],
               a[5]*b[0] - a[2]*b[1],
               a[6]*b[0] + a[1]*b[1],
               a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0],
               a[0]*b[1]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[5] - a[5]*b[4] - a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[5]*b[2] + a[4]*b[1] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[6]*b[2] - a[4]*b[0] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[6]*b[4] - a[5]*b[5] - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[6]*b[3] + a[4]*b[5] - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
               a[5]*b[3] - a[4]*b[4] - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[3]*b[3] - a[2]*b[4] + a[1]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[5] - a[5]*b[4] - a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[5]*b[2] + a[4]*b[1] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[6]*b[2] - a[4]*b[0] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[3]*b[3] - a[2]*b[4] + a[1]*b[5]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [a[1],
             a[2],
             a[3],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[6]*b[3] + a[5]*b[2] + a[4]*b[1] + a[0]*b[0],
               a[6]*b[2] + a[5]*b[3] - a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] - a[4]*b[3] + a[0]*b[2],
               a[3]*b[3] - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] - a[2]*b[3] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2] + a[1]*b[3],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[6]*b[3] + a[5]*b[2] + a[4]*b[1] + a[0]*b[0],
               a[6]*b[2] + a[5]*b[3] - a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] - a[4]*b[3] + a[0]*b[2],
               a[3]*b[3],
             - a[2]*b[3],
               a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [a[4],
             a[5],
             a[6],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[2] - a[5]*b[1] - a[4]*b[0],
             - a[6]*b[3] - a[3]*b[1] - a[2]*b[0],
               a[5]*b[3] - a[3]*b[2] + a[1]*b[0],
             - a[4]*b[3] + a[2]*b[2] + a[1]*b[1],
               a[6]*b[1] - a[5]*b[2] + a[3]*b[3] + a[0]*b[0],
             - a[6]*b[0] + a[4]*b[2] - a[2]*b[3] + a[0]*b[1],
               a[5]*b[0] - a[4]*b[1] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[2] - a[5]*b[1] - a[4]*b[0],
             - a[6]*b[3] - a[3]*b[1] - a[2]*b[0],
               a[5]*b[3] - a[3]*b[2] + a[1]*b[0],
             - a[4]*b[3] + a[2]*b[2] + a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[2]*b[3] + a[0]*b[1],
               a[1]*b[3] + a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[6] - a[5]*b[5] - a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[2] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] - a[4]*b[1] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[6]*b[5] - a[5]*b[6] + a[4]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[6]*b[4] + a[5]*b[0] + a[4]*b[6] - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
               a[6]*b[0] + a[5]*b[4] - a[4]*b[5] - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[6] - a[5]*b[5] - a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[2] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] - a[4]*b[1] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[4]*b[0] + a[0]*b[4],
               a[5]*b[0] + a[0]*b[5],
               a[6]*b[0] + a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [a[0],
             a[1],
             a[2],
             a[3],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[4] + a[5]*b[3] + a[4]*b[2] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] + a[5]*b[4] - a[4]*b[1] + a[2]*b[0] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] - a[4]*b[4] + a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] + a[3]*b[4] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[3]*b[1] - a[2]*b[4] + a[1]*b[3],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3] + a[1]*b[4],
               a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[4] + a[5]*b[3] + a[4]*b[2] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] + a[5]*b[4] - a[4]*b[1] + a[2]*b[0] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] - a[4]*b[4] + a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] + a[3]*b[4],
               a[5]*b[0] - a[2]*b[4],
               a[6]*b[0] + a[1]*b[4],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3],
               a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [a[0],
             a[4],
             a[5],
             a[6],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[3] - a[5]*b[2] - a[4]*b[1] + a[0]*b[0],
             - a[6]*b[4] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[5]*b[4] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[4]*b[4] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[6]*b[2] - a[5]*b[3] + a[4]*b[0] + a[3]*b[4] + a[0]*b[1],
             - a[6]*b[1] + a[5]*b[0] + a[4]*b[3] - a[2]*b[4] + a[0]*b[2],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[1]*b[4] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[3] - a[5]*b[2] - a[4]*b[1] + a[0]*b[0],
             - a[6]*b[4] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[5]*b[4] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[4]*b[4] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[4]*b[0] + a[3]*b[4] + a[0]*b[1],
               a[5]*b[0] - a[2]*b[4] + a[0]*b[2],
               a[6]*b[0] + a[1]*b[4] + a[0]*b[3],
               a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[0]*b[1],
               a[5]*b[0] + a[0]*b[2],
               a[6]*b[0] + a[0]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             a[6],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[5] - a[5]*b[4] - a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[6]*b[6] + a[5]*b[2] + a[4]*b[1] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[6]*b[2] + a[5]*b[6] - a[4]*b[0] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] - a[4]*b[6] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[6]*b[4] - a[5]*b[5] + a[3]*b[6] - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[6]*b[3] + a[4]*b[5] - a[3]*b[0] - a[2]*b[6] + a[1]*b[2] + a[0]*b[4],
               a[5]*b[3] - a[4]*b[4] - a[3]*b[1] + a[2]*b[2] + a[1]*b[6] + a[0]*b[5],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[3]*b[3] - a[2]*b[4] + a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[5] - a[5]*b[4] - a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[6]*b[6] + a[5]*b[2] + a[4]*b[1] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[6]*b[2] + a[5]*b[6] - a[4]*b[0] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] - a[4]*b[6] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[3]*b[6] + a[0]*b[3],
             - a[2]*b[6] + a[0]*b[4],
               a[1]*b[6] + a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[3]*b[3] - a[2]*b[4] + a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_0_1_2<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [a[0],
             a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             a[6],
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[6] - a[5]*b[5] - a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[7] + a[5]*b[3] + a[4]*b[2] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] + a[5]*b[7] - a[4]*b[1] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] - a[4]*b[7] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[6]*b[5] - a[5]*b[6] + a[4]*b[0] + a[3]*b[7] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[6]*b[4] + a[5]*b[0] + a[4]*b[6] - a[3]*b[1] - a[2]*b[7] + a[1]*b[3] + a[0]*b[5],
               a[6]*b[0] + a[5]*b[4] - a[4]*b[5] - a[3]*b[2] + a[2]*b[3] + a[1]*b[7] + a[0]*b[6],
               a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6] + a[0]*b[7]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[6] - a[5]*b[5] - a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[7] + a[5]*b[3] + a[4]*b[2] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] + a[5]*b[7] - a[4]*b[1] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] - a[4]*b[7] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[4]*b[0] + a[3]*b[7] + a[0]*b[4],
               a[5]*b[0] - a[2]*b[7] + a[0]*b[5],
               a[6]*b[0] + a[1]*b[7] + a[0]*b[6],
               a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_0_1_2<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6] + a[0]*b[7]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_0_1_3<T> (
    [T;5]
);

impl<T> VectorSpace for MV3_0_1_3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_0_1_3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_0_1_3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_0_1_3(
            [ self.0[0],
              self.0[1],
              self.0[2],
              self.0[3],
             -self.0[4]]
        )
    }
}

impl<T> MV3_0_1_3<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_0_1_3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3],
             a[4] + b[4]]
        )
    }
}

impl<T> Sub for MV3_0_1_3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3],
             a[4] - b[4]]
        )
    }
}

impl<T> Neg for MV3_0_1_3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_0_1_3 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3],
             -a[4]]
        )
    }
}

impl<T> Mul<T> for MV3_0_1_3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_3 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other,
             a[4] * other]
        )
    }
}

impl<T> Div<T> for MV3_0_1_3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_3 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other,
             a[4] / other]
        )
    }
}

impl<T> Rem<T> for MV3_0_1_3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_3 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other,
             a[4] % other]
        )
    }
}

impl<T> AddAssign for MV3_0_1_3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
        a[4] += b[4];
    }
}

impl<T> SubAssign for MV3_0_1_3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
        a[4] -= b[4];
    }
}

impl<T> MulAssign<T> for MV3_0_1_3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
        a[4] *= other;
    }
}

impl<T> DivAssign<T> for MV3_0_1_3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
        a[4] /= other;
    }
}

impl<T> Zero for MV3_0_1_3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_0_1_3 ([ T::zero(), T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        self.0[4].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_0_1_3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        self.0[4]==other.0[4]&&
        true
    }
}

impl<T> From<T> for MV3_0_1_3<T> where T: Zero {
    fn from(a: T) -> Self {
        MV3_0_1_3 (
            [a,T::zero(),T::zero(),T::zero(),T::zero()]
        )
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[4]*b[2] - a[2]*b[0] + a[1]*b[1],
             - a[4]*b[1] - a[3]*b[0] + a[1]*b[2],
               a[4]*b[0] - a[3]*b[1] + a[2]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[4]*b[2],
             - a[4]*b[1],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [- a[4]*b[2] - a[3]*b[1] - a[2]*b[0],
               a[4]*b[1] - a[3]*b[2] + a[1]*b[0],
             - a[4]*b[0] + a[2]*b[2] + a[1]*b[1],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [- a[4]*b[2] - a[3]*b[1] - a[2]*b[0],
               a[4]*b[1] - a[3]*b[2] + a[1]*b[0],
             - a[4]*b[0] + a[2]*b[2] + a[1]*b[1],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_3(
            [a[4]]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [- a[4]*b[0],
               a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [- a[4]*b[0],
               a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [a[0],
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[3] - a[2]*b[1] + a[1]*b[2],
             - a[4]*b[2] - a[3]*b[1] + a[1]*b[3],
               a[4]*b[1] - a[3]*b[2] + a[2]*b[3],
               a[4]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[3],
             - a[4]*b[2],
               a[4]*b[1],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2],
             - a[3]*b[1] + a[1]*b[3],
             - a[3]*b[2] + a[2]*b[3],
               a[4]*b[0]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[4]*b[3] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[4]*b[2] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[4]*b[1] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
             - a[4]*b[3] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[4]*b[2] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[4]*b[1] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [a[0],
             a[4]]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[1] + a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[3]*b[1],
             - a[2]*b[1],
               a[1]*b[1],
               a[4]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[1] + a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[3]*b[1],
             - a[2]*b[1],
               a[1]*b[1],
               a[4]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [a[1],
             a[2],
             a[3],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[4]*b[5] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[4]*b[4] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[4]*b[3] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[4]*b[2] - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[4]*b[1] - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
               a[4]*b[0] - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[3]*b[3] - a[2]*b[4] + a[1]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[4]*b[5] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[4]*b[4] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[4]*b[3] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[4]*b[2] + a[0]*b[3],
             - a[4]*b[1] + a[0]*b[4],
               a[4]*b[0] + a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[3]*b[3] - a[2]*b[4] + a[1]*b[5]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [a[1],
             a[2],
             a[3],
             a[4]]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[4]*b[2] + a[3]*b[3] - a[2]*b[0] + a[1]*b[1],
             - a[4]*b[1] - a[3]*b[0] - a[2]*b[3] + a[1]*b[2],
               a[4]*b[0] - a[3]*b[1] + a[2]*b[2] + a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[4]*b[2] + a[3]*b[3],
             - a[4]*b[1] - a[2]*b[3],
               a[4]*b[0] + a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[4]]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[3],
             - a[4]*b[2] - a[3]*b[1] - a[2]*b[0],
               a[4]*b[1] - a[3]*b[2] + a[1]*b[0],
             - a[4]*b[0] + a[2]*b[2] + a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[2]*b[3] + a[0]*b[1],
               a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[3],
             - a[4]*b[2] - a[3]*b[1] - a[2]*b[0],
               a[4]*b[1] - a[3]*b[2] + a[1]*b[0],
             - a[4]*b[0] + a[2]*b[2] + a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[2]*b[3] + a[0]*b[1],
               a[1]*b[3] + a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [a[0],
             a[1],
             a[2],
             a[3],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[4]*b[6] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[4]*b[5] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[4]*b[4] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[4]*b[3] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[4]*b[2] - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
               a[4]*b[1] - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[4]*b[0] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[4]*b[6] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[4]*b[5] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[4]*b[4] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[4]*b[3] + a[0]*b[4],
             - a[4]*b[2] + a[0]*b[5],
               a[4]*b[1] + a[0]*b[6],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
             - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[4]*b[0] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6]]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[3] + a[3]*b[4] - a[2]*b[1] + a[1]*b[2],
             - a[4]*b[2] - a[3]*b[1] - a[2]*b[4] + a[1]*b[3],
               a[4]*b[1] - a[3]*b[2] + a[2]*b[3] + a[1]*b[4],
               a[4]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[3] + a[3]*b[4],
             - a[4]*b[2] - a[2]*b[4],
               a[4]*b[1] + a[1]*b[4],
               a[4]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2],
             - a[3]*b[1] + a[1]*b[3],
             - a[3]*b[2] + a[2]*b[3],
               a[4]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             a[4]]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[4] + a[0]*b[0],
             - a[4]*b[3] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[4]*b[2] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[4]*b[1] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[3]*b[4] + a[0]*b[1],
             - a[2]*b[4] + a[0]*b[2],
               a[1]*b[4] + a[0]*b[3],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[4] + a[0]*b[0],
             - a[4]*b[3] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[4]*b[2] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[4]*b[1] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[3]*b[4] + a[0]*b[1],
             - a[2]*b[4] + a[0]*b[2],
               a[1]*b[4] + a[0]*b[3],
               a[4]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [a[1],
             a[2],
             a[3],
             T::zero(),
             T::zero(),
             T::zero(),
             a[4]]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[6] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[4]*b[5] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[4]*b[4] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[4]*b[3] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[4]*b[2] + a[3]*b[6] - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[4]*b[1] - a[3]*b[0] - a[2]*b[6] + a[1]*b[2] + a[0]*b[4],
               a[4]*b[0] - a[3]*b[1] + a[2]*b[2] + a[1]*b[6] + a[0]*b[5],
               a[3]*b[3] - a[2]*b[4] + a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[6] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[4]*b[5] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[4]*b[4] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[4]*b[3] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[4]*b[2] + a[3]*b[6] + a[0]*b[3],
             - a[4]*b[1] - a[2]*b[6] + a[0]*b[4],
               a[4]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[3]*b[3] - a[2]*b[4] + a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_0_1_3<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [a[0],
             a[1],
             a[2],
             a[3],
             T::zero(),
             T::zero(),
             T::zero(),
             a[4]]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[7] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[4]*b[6] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[4]*b[5] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[4]*b[4] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[4]*b[3] + a[3]*b[7] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[4]*b[2] - a[3]*b[1] - a[2]*b[7] + a[1]*b[3] + a[0]*b[5],
               a[4]*b[1] - a[3]*b[2] + a[2]*b[3] + a[1]*b[7] + a[0]*b[6],
               a[4]*b[0] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6] + a[0]*b[7]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[7] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[4]*b[6] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[4]*b[5] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[4]*b[4] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[4]*b[3] + a[3]*b[7] + a[0]*b[4],
             - a[4]*b[2] - a[2]*b[7] + a[0]*b[5],
               a[4]*b[1] + a[1]*b[7] + a[0]*b[6],
               a[4]*b[0] + a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_0_1_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
             - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
             - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[4]*b[0] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6] + a[0]*b[7]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_0_2_3<T> (
    [T;5]
);

impl<T> VectorSpace for MV3_0_2_3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_0_2_3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_0_2_3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_0_2_3(
            [ self.0[0],
             -self.0[1],
             -self.0[2],
             -self.0[3],
             -self.0[4]]
        )
    }
}

impl<T> MV3_0_2_3<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_0_2_3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3],
             a[4] + b[4]]
        )
    }
}

impl<T> Sub for MV3_0_2_3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3],
             a[4] - b[4]]
        )
    }
}

impl<T> Neg for MV3_0_2_3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_0_2_3 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3],
             -a[4]]
        )
    }
}

impl<T> Mul<T> for MV3_0_2_3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_2_3 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other,
             a[4] * other]
        )
    }
}

impl<T> Div<T> for MV3_0_2_3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_2_3 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other,
             a[4] / other]
        )
    }
}

impl<T> Rem<T> for MV3_0_2_3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_2_3 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other,
             a[4] % other]
        )
    }
}

impl<T> AddAssign for MV3_0_2_3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
        a[4] += b[4];
    }
}

impl<T> SubAssign for MV3_0_2_3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
        a[4] -= b[4];
    }
}

impl<T> MulAssign<T> for MV3_0_2_3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
        a[4] *= other;
    }
}

impl<T> DivAssign<T> for MV3_0_2_3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
        a[4] /= other;
    }
}

impl<T> Zero for MV3_0_2_3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_0_2_3 ([ T::zero(), T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        self.0[4].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_0_2_3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        self.0[4]==other.0[4]&&
        true
    }
}

impl<T> From<T> for MV3_0_2_3<T> where T: Zero {
    fn from(a: T) -> Self {
        MV3_0_2_3 (
            [a,T::zero(),T::zero(),T::zero(),T::zero()]
        )
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[4]*b[2],
             - a[4]*b[1],
               a[4]*b[0],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[4]*b[2],
             - a[4]*b[1],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
             - a[4]*b[2],
               a[4]*b[1],
             - a[4]*b[0],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
             - a[4]*b[2],
               a[4]*b[1],
             - a[4]*b[0],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_2(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_3(
            [a[4]]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [- a[4]*b[0],
             - a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_3<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_0_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_3(
            [- a[4]*b[0],
             - a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero()]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[4]*b[3] + a[1]*b[0],
             - a[4]*b[2] + a[2]*b[0],
               a[4]*b[1] + a[3]*b[0],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[4]*b[3] + a[1]*b[0],
             - a[4]*b[2] + a[2]*b[0],
               a[4]*b[1] + a[3]*b[0],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [a[0],
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[4]*b[3],
               a[4]*b[2],
             - a[4]*b[1],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3],
               a[4]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[4]*b[3],
               a[4]*b[2],
             - a[4]*b[1],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [a[0],
             a[4]]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[1] + a[0]*b[0],
             - a[3]*b[1],
               a[2]*b[1],
             - a[1]*b[1],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[1] + a[0]*b[0],
             - a[3]*b[1],
               a[2]*b[1],
             - a[1]*b[1],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
             - a[4]*b[5] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[4] + a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[4]*b[3] - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[4]*b[2] + a[3]*b[4] - a[2]*b[5] + a[0]*b[3],
             - a[4]*b[1] - a[3]*b[3] + a[1]*b[5] + a[0]*b[4],
               a[4]*b[0] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
             - a[4]*b[5] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[4] + a[3]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[4]*b[3] - a[3]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[4]*b[2] + a[0]*b[3],
             - a[4]*b[1] + a[0]*b[4],
               a[4]*b[0] + a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[4]]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[3],
             - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[2]*b[3] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] - a[1]*b[3] + a[0]*b[2],
               a[4]*b[2],
             - a[4]*b[1],
               a[4]*b[0],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[3],
             - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[3]*b[2] + a[2]*b[3] - a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] - a[2]*b[0] - a[1]*b[3] + a[0]*b[2],
               a[4]*b[2],
             - a[4]*b[1],
               a[4]*b[0],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_1_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [a[1],
             a[2],
             a[3],
             a[4]]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[3] - a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
             - a[4]*b[2] - a[3]*b[3],
               a[4]*b[1] + a[2]*b[3],
             - a[4]*b[0] - a[1]*b[3],
               a[3]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[3] - a[3]*b[2] - a[2]*b[1] - a[1]*b[0],
             - a[4]*b[2] - a[3]*b[3],
               a[4]*b[1] + a[2]*b[3],
             - a[4]*b[0] - a[1]*b[3],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[4]*b[6] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[5] + a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[4]*b[4] - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[4]*b[3] + a[3]*b[5] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[4]*b[2] - a[3]*b[4] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[4]*b[1] + a[3]*b[0] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[4]*b[6] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[5] + a[3]*b[3] - a[1]*b[1] + a[0]*b[2],
             - a[4]*b[4] - a[3]*b[2] - a[2]*b[1] + a[0]*b[3],
               a[4]*b[3] + a[1]*b[0] + a[0]*b[4],
             - a[4]*b[2] + a[2]*b[0] + a[0]*b[5],
               a[4]*b[1] + a[3]*b[0] + a[0]*b[6],
               a[4]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[0]*b[5],
               a[3]*b[0] + a[0]*b[6],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             a[4]]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[4] + a[0]*b[0],
             - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] + a[2]*b[4] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[4] + a[0]*b[3],
               a[4]*b[3] + a[1]*b[0],
             - a[4]*b[2] + a[2]*b[0],
               a[4]*b[1] + a[3]*b[0],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[4] + a[0]*b[0],
             - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[3]*b[3] + a[2]*b[4] - a[1]*b[1] + a[0]*b[2],
             - a[3]*b[2] - a[2]*b[1] - a[1]*b[4] + a[0]*b[3],
               a[4]*b[3] + a[1]*b[0],
             - a[4]*b[2] + a[2]*b[0],
               a[4]*b[1] + a[3]*b[0],
               a[4]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[4] - a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[4]*b[3] - a[3]*b[4],
               a[4]*b[2] + a[2]*b[4],
             - a[4]*b[1] - a[1]*b[4],
               a[3]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[3]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3],
               a[4]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[4] - a[3]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
             - a[4]*b[3] - a[3]*b[4],
               a[4]*b[2] + a[2]*b[4],
             - a[4]*b[1] - a[1]*b[4],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_0_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [T::zero(),
             T::zero(),
             T::zero(),
             a[1],
             a[2],
             a[3],
             a[4]]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[6] - a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
             - a[4]*b[5] - a[3]*b[6] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[4] + a[3]*b[2] + a[2]*b[6] - a[1]*b[0] + a[0]*b[1],
             - a[4]*b[3] - a[3]*b[1] - a[2]*b[0] - a[1]*b[6] + a[0]*b[2],
               a[4]*b[2] + a[3]*b[4] - a[2]*b[5] + a[0]*b[3],
             - a[4]*b[1] - a[3]*b[3] + a[1]*b[5] + a[0]*b[4],
               a[4]*b[0] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[6] - a[3]*b[5] - a[2]*b[4] - a[1]*b[3],
             - a[4]*b[5] - a[3]*b[6] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[4] + a[3]*b[2] + a[2]*b[6] - a[1]*b[0] + a[0]*b[1],
             - a[4]*b[3] - a[3]*b[1] - a[2]*b[0] - a[1]*b[6] + a[0]*b[2],
               a[4]*b[2] + a[0]*b[3],
             - a[4]*b[1] + a[0]*b[4],
               a[4]*b[0] + a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[0]*b[4],
               a[0]*b[5],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_0_2_3<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [a[0],
             T::zero(),
             T::zero(),
             T::zero(),
             a[1],
             a[2],
             a[3],
             a[4]]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[7] - a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[4]*b[6] - a[3]*b[7] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[5] + a[3]*b[3] + a[2]*b[7] - a[1]*b[1] + a[0]*b[2],
             - a[4]*b[4] - a[3]*b[2] - a[2]*b[1] - a[1]*b[7] + a[0]*b[3],
               a[4]*b[3] + a[3]*b[5] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[4]*b[2] - a[3]*b[4] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[4]*b[1] + a[3]*b[0] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[7]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[4]*b[7] - a[3]*b[6] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
             - a[4]*b[6] - a[3]*b[7] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[5] + a[3]*b[3] + a[2]*b[7] - a[1]*b[1] + a[0]*b[2],
             - a[4]*b[4] - a[3]*b[2] - a[2]*b[1] - a[1]*b[7] + a[0]*b[3],
               a[4]*b[3] + a[1]*b[0] + a[0]*b[4],
             - a[4]*b[2] + a[2]*b[0] + a[0]*b[5],
               a[4]*b[1] + a[3]*b[0] + a[0]*b[6],
               a[4]*b[0] + a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_0_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[0]*b[3],
               a[1]*b[0] + a[0]*b[4],
               a[2]*b[0] + a[0]*b[5],
               a[3]*b[0] + a[0]*b[6],
               a[4]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[7]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_1_2_3<T> (
    [T;7]
);

impl<T> VectorSpace for MV3_1_2_3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_1_2_3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_1_2_3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_1_2_3(
            [ self.0[0],
              self.0[1],
              self.0[2],
             -self.0[3],
             -self.0[4],
             -self.0[5],
             -self.0[6]]
        )
    }
}

impl<T> MV3_1_2_3<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_1_2_3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3],
             a[4] + b[4],
             a[5] + b[5],
             a[6] + b[6]]
        )
    }
}

impl<T> Sub for MV3_1_2_3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3],
             a[4] - b[4],
             a[5] - b[5],
             a[6] - b[6]]
        )
    }
}

impl<T> Neg for MV3_1_2_3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_1_2_3 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3],
             -a[4],
             -a[5],
             -a[6]]
        )
    }
}

impl<T> Mul<T> for MV3_1_2_3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_2_3 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other,
             a[4] * other,
             a[5] * other,
             a[6] * other]
        )
    }
}

impl<T> Div<T> for MV3_1_2_3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_2_3 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other,
             a[4] / other,
             a[5] / other,
             a[6] / other]
        )
    }
}

impl<T> Rem<T> for MV3_1_2_3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_1_2_3 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other,
             a[4] % other,
             a[5] % other,
             a[6] % other]
        )
    }
}

impl<T> AddAssign for MV3_1_2_3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
        a[4] += b[4];
        a[5] += b[5];
        a[6] += b[6];
    }
}

impl<T> SubAssign for MV3_1_2_3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
        a[4] -= b[4];
        a[5] -= b[5];
        a[6] -= b[6];
    }
}

impl<T> MulAssign<T> for MV3_1_2_3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
        a[4] *= other;
        a[5] *= other;
        a[6] *= other;
    }
}

impl<T> DivAssign<T> for MV3_1_2_3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
        a[4] /= other;
        a[5] /= other;
        a[6] /= other;
    }
}

impl<T> Zero for MV3_1_2_3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_1_2_3 ([ T::zero(), T::zero(), T::zero(), T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        self.0[4].is_zero()&&
        self.0[5].is_zero()&&
        self.0[6].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_1_2_3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        self.0[4]==other.0[4]&&
        self.0[5]==other.0[5]&&
        self.0[6]==other.0[6]&&
        true
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [T::zero()]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[2] + a[3]*b[1],
               a[5]*b[2] - a[3]*b[0],
             - a[5]*b[1] - a[4]*b[0],
               a[6]*b[2] - a[1]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[2]*b[0] + a[0]*b[2],
               a[6]*b[0] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[4]*b[2] + a[3]*b[1],
               a[5]*b[2] - a[3]*b[0],
             - a[5]*b[1] - a[4]*b[0],
               a[6]*b[2],
             - a[6]*b[1],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [a[3],
             a[4],
             a[5]]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[2] - a[4]*b[1] - a[3]*b[0],
             - a[6]*b[2] - a[2]*b[1] - a[1]*b[0],
               a[6]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[6]*b[0] + a[1]*b[2] + a[0]*b[1],
               a[5]*b[1] - a[4]*b[2],
             - a[5]*b[0] + a[3]*b[2],
               a[4]*b[0] - a[3]*b[1],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_0_1<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1(
            [- a[5]*b[2] - a[4]*b[1] - a[3]*b[0],
             - a[6]*b[2] - a[2]*b[1] - a[1]*b[0],
               a[6]*b[1] - a[2]*b[2] + a[0]*b[0],
             - a[6]*b[0] + a[1]*b[2] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_3(
            [a[6]]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[0],
             - a[5]*b[0],
               a[4]*b[0],
             - a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[0],
             - a[5]*b[0],
               a[4]*b[0],
             - a[3]*b[0],
               a[2]*b[0],
             - a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3_3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [T::zero(),
             a[0],
             a[1],
             a[2]]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[3] + a[3]*b[2] + a[0]*b[0],
               a[5]*b[3] - a[3]*b[1] + a[1]*b[0],
             - a[5]*b[2] - a[4]*b[1] + a[2]*b[0],
               a[6]*b[3] + a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
             - a[6]*b[2] + a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[6]*b[1] + a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[3]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[4]*b[3] + a[3]*b[2] + a[0]*b[0],
               a[5]*b[3] - a[3]*b[1] + a[1]*b[0],
             - a[5]*b[2] - a[4]*b[1] + a[2]*b[0],
               a[6]*b[3] + a[3]*b[0],
             - a[6]*b[2] + a[4]*b[0],
               a[6]*b[1] + a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[3]*b[3]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [T::zero(),
             a[3],
             a[4],
             a[5]]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[3] - a[4]*b[2] - a[3]*b[1],
             - a[6]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[6]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[6]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[5]*b[2] - a[4]*b[3] + a[3]*b[0],
             - a[5]*b[1] + a[4]*b[0] + a[3]*b[3],
               a[5]*b[0] + a[4]*b[1] - a[3]*b[2],
               a[6]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[3] - a[4]*b[2] - a[3]*b[1],
             - a[6]*b[3] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[6]*b[2] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[6]*b[1] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [T::zero(),
             a[6]]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[1],
             - a[5]*b[1] + a[0]*b[0],
               a[4]*b[1] + a[1]*b[0],
             - a[3]*b[1] + a[2]*b[0],
               a[3]*b[0] + a[2]*b[1],
               a[4]*b[0] - a[1]*b[1],
               a[5]*b[0] + a[0]*b[1],
               a[6]*b[0]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[1],
             - a[5]*b[1] + a[0]*b[0],
               a[4]*b[1] + a[1]*b[0],
             - a[3]*b[1] + a[2]*b[0],
               a[3]*b[0] + a[2]*b[1],
               a[4]*b[0] - a[1]*b[1],
               a[5]*b[0] + a[0]*b[1],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [a[0],
             a[1],
             a[2],
             a[3],
             a[4],
             a[5]]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[5] - a[4]*b[4] - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[5] + a[4]*b[2] + a[3]*b[1] - a[2]*b[4] - a[1]*b[3],
               a[6]*b[4] + a[5]*b[2] - a[3]*b[0] - a[2]*b[5] + a[0]*b[3],
             - a[6]*b[3] - a[5]*b[1] - a[4]*b[0] + a[1]*b[5] + a[0]*b[4],
               a[6]*b[2] + a[5]*b[4] - a[4]*b[5] - a[1]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[3] + a[3]*b[5] - a[2]*b[0] + a[0]*b[2],
               a[6]*b[0] + a[4]*b[3] - a[3]*b[4] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[5]*b[5] - a[4]*b[4] - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[5] + a[4]*b[2] + a[3]*b[1] - a[2]*b[4] - a[1]*b[3],
               a[6]*b[4] + a[5]*b[2] - a[3]*b[0] - a[2]*b[5] + a[0]*b[3],
             - a[6]*b[3] - a[5]*b[1] - a[4]*b[0] + a[1]*b[5] + a[0]*b[4],
               a[6]*b[2],
             - a[6]*b[1],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [a[0],
             a[1],
             a[2],
             a[6]]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[5]*b[3] + a[4]*b[2] + a[3]*b[1],
               a[5]*b[2] + a[4]*b[3] - a[3]*b[0],
             - a[5]*b[1] - a[4]*b[0] - a[3]*b[3],
               a[6]*b[2] + a[2]*b[3] - a[1]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[2]*b[0] - a[1]*b[3] + a[0]*b[2],
               a[6]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[5]*b[3] + a[4]*b[2] + a[3]*b[1],
               a[5]*b[2] + a[4]*b[3] - a[3]*b[0],
             - a[5]*b[1] - a[4]*b[0] - a[3]*b[3],
               a[6]*b[2] + a[2]*b[3],
             - a[6]*b[1] - a[1]*b[3],
               a[6]*b[0] + a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [a[3],
             a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[3] - a[5]*b[2] - a[4]*b[1] - a[3]*b[0],
             - a[6]*b[2] - a[5]*b[3] - a[2]*b[1] - a[1]*b[0],
               a[6]*b[1] + a[4]*b[3] - a[2]*b[2] + a[0]*b[0],
             - a[6]*b[0] - a[3]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[5]*b[1] - a[4]*b[2] + a[2]*b[3],
             - a[5]*b[0] + a[3]*b[2] - a[1]*b[3],
               a[4]*b[0] - a[3]*b[1] + a[0]*b[3],
               a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[3] - a[5]*b[2] - a[4]*b[1] - a[3]*b[0],
             - a[6]*b[2] - a[5]*b[3] - a[2]*b[1] - a[1]*b[0],
               a[6]*b[1] + a[4]*b[3] - a[2]*b[2] + a[0]*b[0],
             - a[6]*b[0] - a[3]*b[3] + a[1]*b[2] + a[0]*b[1],
               a[2]*b[3],
             - a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[2]*b[0] - a[1]*b[1] + a[0]*b[2]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             a[3],
             a[4],
             a[5]]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[6] - a[4]*b[5] - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[6]*b[6] + a[4]*b[3] + a[3]*b[2] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[6]*b[5] + a[5]*b[3] - a[3]*b[1] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[6]*b[4] - a[5]*b[2] - a[4]*b[1] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[6]*b[3] + a[5]*b[5] - a[4]*b[6] + a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[4] + a[4]*b[0] + a[3]*b[6] - a[2]*b[1] + a[0]*b[3],
               a[6]*b[1] + a[5]*b[0] + a[4]*b[4] - a[3]*b[5] - a[2]*b[2] + a[1]*b[3],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[3]*b[3] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[5]*b[6] - a[4]*b[5] - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[6]*b[6] + a[4]*b[3] + a[3]*b[2] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[6]*b[5] + a[5]*b[3] - a[3]*b[1] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[6]*b[4] - a[5]*b[2] - a[4]*b[1] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[6]*b[3] + a[3]*b[0],
             - a[6]*b[2] + a[4]*b[0],
               a[6]*b[1] + a[5]*b[0],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[3]*b[3] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             a[6]]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[5]*b[4] + a[4]*b[3] + a[3]*b[2] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[4] - a[3]*b[1] + a[1]*b[0],
             - a[5]*b[2] - a[4]*b[1] - a[3]*b[4] + a[2]*b[0],
               a[6]*b[3] + a[3]*b[0] + a[2]*b[4] - a[1]*b[1] + a[0]*b[2],
             - a[6]*b[2] + a[4]*b[0] - a[2]*b[1] - a[1]*b[4] + a[0]*b[3],
               a[6]*b[1] + a[5]*b[0] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[3]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[5]*b[4] + a[4]*b[3] + a[3]*b[2] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[4] - a[3]*b[1] + a[1]*b[0],
             - a[5]*b[2] - a[4]*b[1] - a[3]*b[4] + a[2]*b[0],
               a[6]*b[3] + a[3]*b[0] + a[2]*b[4],
             - a[6]*b[2] + a[4]*b[0] - a[1]*b[4],
               a[6]*b[1] + a[5]*b[0] + a[0]*b[4],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[3]*b[3]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [T::zero(),
             a[3],
             a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[4] - a[5]*b[3] - a[4]*b[2] - a[3]*b[1],
             - a[6]*b[3] - a[5]*b[4] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[6]*b[2] + a[4]*b[4] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[3]*b[4] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[5]*b[2] - a[4]*b[3] + a[3]*b[0] + a[2]*b[4],
             - a[5]*b[1] + a[4]*b[0] + a[3]*b[3] - a[1]*b[4],
               a[5]*b[0] + a[4]*b[1] - a[3]*b[2] + a[0]*b[4],
               a[6]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[4] - a[5]*b[3] - a[4]*b[2] - a[3]*b[1],
             - a[6]*b[3] - a[5]*b[4] - a[2]*b[2] - a[1]*b[1] + a[0]*b[0],
               a[6]*b[2] + a[4]*b[4] - a[2]*b[3] + a[1]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[3]*b[4] + a[2]*b[0] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] + a[2]*b[4],
               a[4]*b[0] - a[1]*b[4],
               a[5]*b[0] + a[0]*b[4],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0] + a[2]*b[1] - a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[6] - a[5]*b[5] - a[4]*b[4] - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[5] - a[5]*b[6] + a[4]*b[2] + a[3]*b[1] - a[2]*b[4] - a[1]*b[3],
               a[6]*b[4] + a[5]*b[2] + a[4]*b[6] - a[3]*b[0] - a[2]*b[5] + a[0]*b[3],
             - a[6]*b[3] - a[5]*b[1] - a[4]*b[0] - a[3]*b[6] + a[1]*b[5] + a[0]*b[4],
               a[6]*b[2] + a[5]*b[4] - a[4]*b[5] + a[2]*b[6] - a[1]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[3] + a[3]*b[5] - a[2]*b[0] - a[1]*b[6] + a[0]*b[2],
               a[6]*b[0] + a[4]*b[3] - a[3]*b[4] - a[2]*b[1] + a[1]*b[2] + a[0]*b[6],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[6] - a[5]*b[5] - a[4]*b[4] - a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[5] - a[5]*b[6] + a[4]*b[2] + a[3]*b[1] - a[2]*b[4] - a[1]*b[3],
               a[6]*b[4] + a[5]*b[2] + a[4]*b[6] - a[3]*b[0] - a[2]*b[5] + a[0]*b[3],
             - a[6]*b[3] - a[5]*b[1] - a[4]*b[0] - a[3]*b[6] + a[1]*b[5] + a[0]*b[4],
               a[6]*b[2] + a[2]*b[6],
             - a[6]*b[1] - a[1]*b[6],
               a[6]*b[0] + a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [- a[1]*b[0] + a[0]*b[1],
             - a[2]*b[0] + a[0]*b[2],
             - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[4]*b[1] + a[3]*b[2] + a[2]*b[3] - a[1]*b[4] + a[0]*b[5]]
)
    }
}

impl<T> From<MV3_1_2_3<T>> for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2_3(
            [T::zero(),
             a[0],
             a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[7] - a[5]*b[6] - a[4]*b[5] - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[6]*b[6] - a[5]*b[7] + a[4]*b[3] + a[3]*b[2] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[6]*b[5] + a[5]*b[3] + a[4]*b[7] - a[3]*b[1] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[6]*b[4] - a[5]*b[2] - a[4]*b[1] - a[3]*b[7] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[6]*b[3] + a[5]*b[5] - a[4]*b[6] + a[3]*b[0] + a[2]*b[7] - a[1]*b[1] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[4] + a[4]*b[0] + a[3]*b[6] - a[2]*b[1] - a[1]*b[7] + a[0]*b[3],
               a[6]*b[1] + a[5]*b[0] + a[4]*b[4] - a[3]*b[5] - a[2]*b[2] + a[1]*b[3] + a[0]*b[7],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[3]*b[3] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[7] - a[5]*b[6] - a[4]*b[5] - a[3]*b[4] + a[2]*b[3] + a[1]*b[2] + a[0]*b[1],
             - a[6]*b[6] - a[5]*b[7] + a[4]*b[3] + a[3]*b[2] - a[2]*b[5] - a[1]*b[4] + a[0]*b[0],
               a[6]*b[5] + a[5]*b[3] + a[4]*b[7] - a[3]*b[1] - a[2]*b[6] + a[1]*b[0] + a[0]*b[4],
             - a[6]*b[4] - a[5]*b[2] - a[4]*b[1] - a[3]*b[7] + a[2]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[6]*b[3] + a[3]*b[0] + a[2]*b[7],
             - a[6]*b[2] + a[4]*b[0] - a[1]*b[7],
               a[6]*b[1] + a[5]*b[0] + a[0]*b[7],
               a[6]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0] - a[1]*b[1] + a[0]*b[2],
               a[4]*b[0] - a[2]*b[1] + a[0]*b[3],
               a[5]*b[0] - a[2]*b[2] + a[1]*b[3],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[3]*b[3] + a[2]*b[4] - a[1]*b[5] + a[0]*b[6]]
)
    }
}

#[derive(Copy,Clone,Debug)]
pub struct MV3_0_1_2_3<T> (
    [T;8]
);

impl<T> VectorSpace for MV3_0_1_2_3<T> where T: Num+Copy {
    type Scalar = T;
}

impl<T> InnerSpace for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    fn dot(self, other: Self) -> T {
        let result = self.inner(other).0[0];
        if result < T::zero() {
            -result
        } else {
            result
        }
    }
}

/*impl<T> MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy+PartialOrd {
    pub fn magnitude2(self) -> T {
        let mag = self.inner(self).0[0];
        if mag < T::zero() {
            -mag
        } else {
            mag
        }
    }
}*/

impl<T> MV3_0_1_2_3<T> where T: Real {
/*    pub fn magnitude(self) -> T {
        self.magnitude2().sqrt()
    }
    pub fn normalize(self) -> Self {
        self/self.magnitude()
    }*/
    pub fn inverse(self) -> Self {
        self.reverse()/self.magnitude2()
    }
    pub fn rotate<MV>(self, other: MV) -> MV where Self: Mul<MV>, <Self as Mul<MV>>::Output: Mul<Self>, MV: From<<<Self as Mul<MV>>::Output as Mul<Self>>::Output> {
        (self*other*self.inverse()).into()
    }
}

impl<T> MV3_0_1_2_3<T> where T: Neg<Output=T>+Copy {
    pub fn reverse(self) -> Self {
        MV3_0_1_2_3(
            [ self.0[0],
              self.0[1],
              self.0[2],
              self.0[3],
             -self.0[4],
             -self.0[5],
             -self.0[6],
             -self.0[7]]
        )
    }
}

impl<T> MV3_0_1_2_3<T> where T: Num+AddAssign+Copy+Neg<Output=T> {
    pub fn exp(self) -> MV3_0_1_2_3<T> {
        let mut z = MV3_0_1_2_3::one();
        let mut k0 = T::zero();
        let mut k = T::one();
        let mut result = MV3_0_1_2_3::zero();
        result += z/k;
        for _ in 0..15 {
            z = z*self;
            k0 += T::one();
            k = k * k0;

            result += z/k;
        }
        result
    }
}

impl<T> Add for MV3_0_1_2_3<T> where T: Add<Output=T>+Copy {
    type Output = Self;
    fn add(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3 (
            [a[0] + b[0],
             a[1] + b[1],
             a[2] + b[2],
             a[3] + b[3],
             a[4] + b[4],
             a[5] + b[5],
             a[6] + b[6],
             a[7] + b[7]]
        )
    }
}

impl<T> Sub for MV3_0_1_2_3<T> where T: Sub<Output=T>+Copy {
    type Output = Self;
    fn sub(self,  other: Self) -> Self {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3 (
            [a[0] - b[0],
             a[1] - b[1],
             a[2] - b[2],
             a[3] - b[3],
             a[4] - b[4],
             a[5] - b[5],
             a[6] - b[6],
             a[7] - b[7]]
        )
    }
}

impl<T> Neg for MV3_0_1_2_3<T> where T: Neg<Output=T>+Copy {
    type Output = Self;
    fn neg(self) -> Self {
        let a = self.0;
        MV3_0_1_2_3 (
            [-a[0],
             -a[1],
             -a[2],
             -a[3],
             -a[4],
             -a[5],
             -a[6],
             -a[7]]
        )
    }
}

impl<T> Mul<T> for MV3_0_1_2_3<T> where T: Mul<Output=T>+Copy {
    type Output = Self;
    fn mul(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_2_3 (
            [a[0] * other,
             a[1] * other,
             a[2] * other,
             a[3] * other,
             a[4] * other,
             a[5] * other,
             a[6] * other,
             a[7] * other]
        )
    }
}

impl<T> Div<T> for MV3_0_1_2_3<T> where T: Div<Output=T>+Copy {
    type Output = Self;
    fn div(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_2_3 (
            [a[0] / other,
             a[1] / other,
             a[2] / other,
             a[3] / other,
             a[4] / other,
             a[5] / other,
             a[6] / other,
             a[7] / other]
        )
    }
}

impl<T> Rem<T> for MV3_0_1_2_3<T> where T: Rem<Output=T>+Copy {
    type Output = Self;
    fn rem(self,  other: T) -> Self {
        let a = self.0;
        MV3_0_1_2_3 (
            [a[0] % other,
             a[1] % other,
             a[2] % other,
             a[3] % other,
             a[4] % other,
             a[5] % other,
             a[6] % other,
             a[7] % other]
        )
    }
}

impl<T> AddAssign for MV3_0_1_2_3<T> where T: AddAssign+Copy {
    fn add_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] += b[0];
        a[1] += b[1];
        a[2] += b[2];
        a[3] += b[3];
        a[4] += b[4];
        a[5] += b[5];
        a[6] += b[6];
        a[7] += b[7];
    }
}

impl<T> SubAssign for MV3_0_1_2_3<T> where T: SubAssign+Copy {
    fn sub_assign(&mut self, other: Self) {
        let ref mut a = self.0;
        let b = other.0;
        a[0] -= b[0];
        a[1] -= b[1];
        a[2] -= b[2];
        a[3] -= b[3];
        a[4] -= b[4];
        a[5] -= b[5];
        a[6] -= b[6];
        a[7] -= b[7];
    }
}

impl<T> MulAssign<T> for MV3_0_1_2_3<T> where T: MulAssign+Copy {
    fn mul_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] *= other;
        a[1] *= other;
        a[2] *= other;
        a[3] *= other;
        a[4] *= other;
        a[5] *= other;
        a[6] *= other;
        a[7] *= other;
    }
}

impl<T> DivAssign<T> for MV3_0_1_2_3<T> where T: DivAssign+Copy {
    fn div_assign(&mut self, other: T) {
        let ref mut a = self.0;
        a[0] /= other;
        a[1] /= other;
        a[2] /= other;
        a[3] /= other;
        a[4] /= other;
        a[5] /= other;
        a[6] /= other;
        a[7] /= other;
    }
}

impl<T> Zero for MV3_0_1_2_3<T> where T: Zero+Copy {
    fn zero() -> Self { MV3_0_1_2_3 ([ T::zero(), T::zero(), T::zero(), T::zero(), T::zero(), T::zero(), T::zero(), T::zero()]) }
    fn is_zero(&self) -> bool {
        self.0[0].is_zero()&&
        self.0[1].is_zero()&&
        self.0[2].is_zero()&&
        self.0[3].is_zero()&&
        self.0[4].is_zero()&&
        self.0[5].is_zero()&&
        self.0[6].is_zero()&&
        self.0[7].is_zero()&&
        true
    }
}

impl<T> PartialEq for MV3_0_1_2_3<T> where T: PartialEq {
    fn eq(&self, other: &Self) -> bool {
        self.0[0]==other.0[0]&&
        self.0[1]==other.0[1]&&
        self.0[2]==other.0[2]&&
        self.0[3]==other.0[3]&&
        self.0[4]==other.0[4]&&
        self.0[5]==other.0[5]&&
        self.0[6]==other.0[6]&&
        self.0[7]==other.0[7]&&
        true
    }
}

impl<T> One for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    fn one() -> Self { MV3_0_1_2_3 ([T::one(),T::zero(),T::zero(),T::zero(),T::zero(),T::zero(),T::zero(),T::zero()]) }
}

impl<T> From<T> for MV3_0_1_2_3<T> where T: Zero {
    fn from(a: T) -> Self {
        MV3_0_1_2_3 (
            [a,T::zero(),T::zero(),T::zero(),T::zero(),T::zero(),T::zero(),T::zero()]
        )
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        MV3(
            []
)
    }
}

impl<T> Mul<MV3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn mul(self,  _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Inner<MV3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn inner(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> Outer<MV3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3<T>;
    fn outer(self, _: MV3<T>) -> MV3<T> {
        MV3(
            []
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_0<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0(
            [a[0]]
)
    }
}

impl<T> Mul<MV3_0<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0],
               a[7]*b[0]]
)
    }
}

impl<T> Inner<MV3_0<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0],
               a[7]*b[0]]
)
    }
}

impl<T> Outer<MV3_0<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0],
               a[7]*b[0]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_1(
            [a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_1<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[5]*b[2] + a[4]*b[1] + a[0]*b[0],
               a[6]*b[2] - a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] + a[0]*b[2],
               a[7]*b[2] - a[2]*b[0] + a[1]*b[1],
             - a[7]*b[1] - a[3]*b[0] + a[1]*b[2],
               a[7]*b[0] - a[3]*b[1] + a[2]*b[2],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2]]
)
    }
}

impl<T> Inner<MV3_1<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [  a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
               a[5]*b[2] + a[4]*b[1] + a[0]*b[0],
               a[6]*b[2] - a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] + a[0]*b[2],
               a[7]*b[2],
             - a[7]*b[1],
               a[7]*b[0]]
)
    }
}

impl<T> Outer<MV3_1<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_2(
            [a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[2] - a[5]*b[1] - a[4]*b[0],
             - a[7]*b[2] - a[3]*b[1] - a[2]*b[0],
               a[7]*b[1] - a[3]*b[2] + a[1]*b[0],
             - a[7]*b[0] + a[2]*b[2] + a[1]*b[1],
               a[6]*b[1] - a[5]*b[2] + a[0]*b[0],
             - a[6]*b[0] + a[4]*b[2] + a[0]*b[1],
               a[5]*b[0] - a[4]*b[1] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> Inner<MV3_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[2] - a[5]*b[1] - a[4]*b[0],
             - a[7]*b[2] - a[3]*b[1] - a[2]*b[0],
               a[7]*b[1] - a[3]*b[2] + a[1]*b[0],
             - a[7]*b[0] + a[2]*b[2] + a[1]*b[1],
               a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2]]
)
    }
}

impl<T> Outer<MV3_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_3(
            [a[7]]
)
    }
}

impl<T> Mul<MV3_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[0],
             - a[6]*b[0],
               a[5]*b[0],
             - a[4]*b[0],
               a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Inner<MV3_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[0],
             - a[6]*b[0],
               a[5]*b[0],
             - a[4]*b[0],
               a[3]*b[0],
             - a[2]*b[0],
               a[1]*b[0],
               a[0]*b[0]]
)
    }
}

impl<T> Outer<MV3_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_3<T>;
    fn outer(self, other: MV3_3<T>) -> MV3_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_3(
            [  a[0]*b[0]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_0_1<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1(
            [a[0],
             a[1],
             a[2],
             a[3]]
)
    }
}

impl<T> Mul<MV3_0_1<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[2] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] - a[4]*b[1] + a[2]*b[0] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] + a[3]*b[0] + a[0]*b[3],
               a[7]*b[3] + a[4]*b[0] - a[2]*b[1] + a[1]*b[2],
             - a[7]*b[2] + a[5]*b[0] - a[3]*b[1] + a[1]*b[3],
               a[7]*b[1] + a[6]*b[0] - a[3]*b[2] + a[2]*b[3],
               a[7]*b[0] + a[6]*b[1] - a[5]*b[2] + a[4]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_1<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
               a[5]*b[3] + a[4]*b[2] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] - a[4]*b[1] + a[2]*b[0] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] + a[3]*b[0] + a[0]*b[3],
               a[7]*b[3] + a[4]*b[0],
             - a[7]*b[2] + a[5]*b[0],
               a[7]*b[1] + a[6]*b[0],
               a[7]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3],
               a[7]*b[0] + a[6]*b[1] - a[5]*b[2] + a[4]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_0_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_2(
            [a[0],
             a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_0_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[3] - a[5]*b[2] - a[4]*b[1] + a[0]*b[0],
             - a[7]*b[3] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[7]*b[2] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[7]*b[1] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[6]*b[2] - a[5]*b[3] + a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] + a[5]*b[0] + a[4]*b[3] + a[0]*b[2],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[0]*b[3],
               a[7]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> Inner<MV3_0_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[3] - a[5]*b[2] - a[4]*b[1] + a[0]*b[0],
             - a[7]*b[3] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[7]*b[2] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[7]*b[1] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[4]*b[0] + a[0]*b[1],
               a[5]*b[0] + a[0]*b[2],
               a[6]*b[0] + a[0]*b[3],
               a[7]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[0]*b[1],
               a[5]*b[0] + a[0]*b[2],
               a[6]*b[0] + a[0]*b[3],
               a[7]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_0_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_3(
            [a[0],
             a[7]]
)
    }
}

impl<T> Mul<MV3_0_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[1] + a[0]*b[0],
             - a[6]*b[1] + a[1]*b[0],
               a[5]*b[1] + a[2]*b[0],
             - a[4]*b[1] + a[3]*b[0],
               a[4]*b[0] + a[3]*b[1],
               a[5]*b[0] - a[2]*b[1],
               a[6]*b[0] + a[1]*b[1],
               a[7]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Inner<MV3_0_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[1] + a[0]*b[0],
             - a[6]*b[1] + a[1]*b[0],
               a[5]*b[1] + a[2]*b[0],
             - a[4]*b[1] + a[3]*b[0],
               a[4]*b[0] + a[3]*b[1],
               a[5]*b[0] - a[2]*b[1],
               a[6]*b[0] + a[1]*b[1],
               a[7]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> Outer<MV3_0_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0],
               a[5]*b[0],
               a[6]*b[0],
               a[7]*b[0] + a[0]*b[1]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_2(
            [a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_1_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[5] - a[5]*b[4] - a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[7]*b[5] + a[5]*b[2] + a[4]*b[1] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[7]*b[4] + a[6]*b[2] - a[4]*b[0] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[7]*b[3] - a[6]*b[1] - a[5]*b[0] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[7]*b[2] + a[6]*b[4] - a[5]*b[5] - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[7]*b[1] - a[6]*b[3] + a[4]*b[5] - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
               a[7]*b[0] + a[5]*b[3] - a[4]*b[4] - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[3]*b[3] - a[2]*b[4] + a[1]*b[5]]
)
    }
}

impl<T> Inner<MV3_1_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2<T>;
    fn inner(self, other: MV3_1_2<T>) -> MV3_0_1_2<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2(
            [- a[6]*b[5] - a[5]*b[4] - a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[7]*b[5] + a[5]*b[2] + a[4]*b[1] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[7]*b[4] + a[6]*b[2] - a[4]*b[0] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[7]*b[3] - a[6]*b[1] - a[5]*b[0] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[7]*b[2] + a[0]*b[3],
             - a[7]*b[1] + a[0]*b[4],
               a[7]*b[0] + a[0]*b[5]]
)
    }
}

impl<T> Outer<MV3_1_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[3]*b[3] - a[2]*b[4] + a[1]*b[5]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_3(
            [a[1],
             a[2],
             a[3],
             a[7]]
)
    }
}

impl<T> Mul<MV3_1_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[6]*b[3] + a[5]*b[2] + a[4]*b[1] + a[0]*b[0],
               a[6]*b[2] + a[5]*b[3] - a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] - a[4]*b[3] + a[0]*b[2],
               a[7]*b[2] + a[3]*b[3] - a[2]*b[0] + a[1]*b[1],
             - a[7]*b[1] - a[3]*b[0] - a[2]*b[3] + a[1]*b[2],
               a[7]*b[0] - a[3]*b[1] + a[2]*b[2] + a[1]*b[3],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_1_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[6]*b[3] + a[5]*b[2] + a[4]*b[1] + a[0]*b[0],
               a[6]*b[2] + a[5]*b[3] - a[4]*b[0] + a[0]*b[1],
             - a[6]*b[1] - a[5]*b[0] - a[4]*b[3] + a[0]*b[2],
               a[7]*b[2] + a[3]*b[3],
             - a[7]*b[1] - a[2]*b[3],
               a[7]*b[0] + a[1]*b[3],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_1_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1],
             - a[3]*b[0] + a[1]*b[2],
             - a[3]*b[1] + a[2]*b[2],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_2_3(
            [a[4],
             a[5],
             a[6],
             a[7]]
)
    }
}

impl<T> Mul<MV3_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[3] - a[6]*b[2] - a[5]*b[1] - a[4]*b[0],
             - a[7]*b[2] - a[6]*b[3] - a[3]*b[1] - a[2]*b[0],
               a[7]*b[1] + a[5]*b[3] - a[3]*b[2] + a[1]*b[0],
             - a[7]*b[0] - a[4]*b[3] + a[2]*b[2] + a[1]*b[1],
               a[6]*b[1] - a[5]*b[2] + a[3]*b[3] + a[0]*b[0],
             - a[6]*b[0] + a[4]*b[2] - a[2]*b[3] + a[0]*b[1],
               a[5]*b[0] - a[4]*b[1] + a[1]*b[3] + a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> Inner<MV3_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[3] - a[6]*b[2] - a[5]*b[1] - a[4]*b[0],
             - a[7]*b[2] - a[6]*b[3] - a[3]*b[1] - a[2]*b[0],
               a[7]*b[1] + a[5]*b[3] - a[3]*b[2] + a[1]*b[0],
             - a[7]*b[0] - a[4]*b[3] + a[2]*b[2] + a[1]*b[1],
               a[3]*b[3] + a[0]*b[0],
             - a[2]*b[3] + a[0]*b[1],
               a[1]*b[3] + a[0]*b[2],
               a[0]*b[3]]
)
    }
}

impl<T> Outer<MV3_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_2_3<T>;
    fn outer(self, other: MV3_2_3<T>) -> MV3_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
               a[3]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[3]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_0_1_2<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_2(
            [a[0],
             a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             a[6]]
)
    }
}

impl<T> Mul<MV3_0_1_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[6] - a[5]*b[5] - a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[7]*b[6] + a[5]*b[3] + a[4]*b[2] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[7]*b[5] + a[6]*b[3] - a[4]*b[1] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[7]*b[4] - a[6]*b[2] - a[5]*b[1] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[7]*b[3] + a[6]*b[5] - a[5]*b[6] + a[4]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[7]*b[2] - a[6]*b[4] + a[5]*b[0] + a[4]*b[6] - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
               a[7]*b[1] + a[6]*b[0] + a[5]*b[4] - a[4]*b[5] - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[7]*b[0] + a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6]]
)
    }
}

impl<T> Inner<MV3_0_1_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[6]*b[6] - a[5]*b[5] - a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[7]*b[6] + a[5]*b[3] + a[4]*b[2] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[7]*b[5] + a[6]*b[3] - a[4]*b[1] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[7]*b[4] - a[6]*b[2] - a[5]*b[1] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[7]*b[3] + a[4]*b[0] + a[0]*b[4],
             - a[7]*b[2] + a[5]*b[0] + a[0]*b[5],
               a[7]*b[1] + a[6]*b[0] + a[0]*b[6],
               a[7]*b[0]]
)
    }
}

impl<T> Outer<MV3_0_1_2<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[7]*b[0] + a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_0_1_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_1_3(
            [a[0],
             a[1],
             a[2],
             a[3],
             a[7]]
)
    }
}

impl<T> Mul<MV3_0_1_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[4] + a[5]*b[3] + a[4]*b[2] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] + a[5]*b[4] - a[4]*b[1] + a[2]*b[0] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] - a[4]*b[4] + a[3]*b[0] + a[0]*b[3],
               a[7]*b[3] + a[4]*b[0] + a[3]*b[4] - a[2]*b[1] + a[1]*b[2],
             - a[7]*b[2] + a[5]*b[0] - a[3]*b[1] - a[2]*b[4] + a[1]*b[3],
               a[7]*b[1] + a[6]*b[0] - a[3]*b[2] + a[2]*b[3] + a[1]*b[4],
               a[7]*b[0] + a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_1_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[6]*b[4] + a[5]*b[3] + a[4]*b[2] + a[1]*b[0] + a[0]*b[1],
               a[6]*b[3] + a[5]*b[4] - a[4]*b[1] + a[2]*b[0] + a[0]*b[2],
             - a[6]*b[2] - a[5]*b[1] - a[4]*b[4] + a[3]*b[0] + a[0]*b[3],
               a[7]*b[3] + a[4]*b[0] + a[3]*b[4],
             - a[7]*b[2] + a[5]*b[0] - a[2]*b[4],
               a[7]*b[1] + a[6]*b[0] + a[1]*b[4],
               a[7]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_1_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3],
               a[7]*b[0] + a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_0_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_0_2_3(
            [a[0],
             a[4],
             a[5],
             a[6],
             a[7]]
)
    }
}

impl<T> Mul<MV3_0_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[4] - a[6]*b[3] - a[5]*b[2] - a[4]*b[1] + a[0]*b[0],
             - a[7]*b[3] - a[6]*b[4] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[7]*b[2] + a[5]*b[4] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[7]*b[1] - a[4]*b[4] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[6]*b[2] - a[5]*b[3] + a[4]*b[0] + a[3]*b[4] + a[0]*b[1],
             - a[6]*b[1] + a[5]*b[0] + a[4]*b[3] - a[2]*b[4] + a[0]*b[2],
               a[6]*b[0] + a[5]*b[1] - a[4]*b[2] + a[1]*b[4] + a[0]*b[3],
               a[7]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> Inner<MV3_0_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[4] - a[6]*b[3] - a[5]*b[2] - a[4]*b[1] + a[0]*b[0],
             - a[7]*b[3] - a[6]*b[4] - a[3]*b[2] - a[2]*b[1] + a[1]*b[0],
               a[7]*b[2] + a[5]*b[4] - a[3]*b[3] + a[2]*b[0] + a[1]*b[1],
             - a[7]*b[1] - a[4]*b[4] + a[3]*b[0] + a[2]*b[3] + a[1]*b[2],
               a[4]*b[0] + a[3]*b[4] + a[0]*b[1],
               a[5]*b[0] - a[2]*b[4] + a[0]*b[2],
               a[6]*b[0] + a[1]*b[4] + a[0]*b[3],
               a[7]*b[0] + a[0]*b[4]]
)
    }
}

impl<T> Outer<MV3_0_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0],
               a[2]*b[0],
               a[3]*b[0],
               a[4]*b[0] + a[0]*b[1],
               a[5]*b[0] + a[0]*b[2],
               a[6]*b[0] + a[0]*b[3],
               a[7]*b[0] + a[3]*b[1] - a[2]*b[2] + a[1]*b[3] + a[0]*b[4]]
)
    }
}

impl<T> From<MV3_0_1_2_3<T>> for MV3_1_2_3<T> where T: Zero+Copy {
    fn from(a: MV3_0_1_2_3<T>) -> Self {
        let a = a.0;
        MV3_1_2_3(
            [a[1],
             a[2],
             a[3],
             a[4],
             a[5],
             a[6],
             a[7]]
)
    }
}

impl<T> Mul<MV3_1_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[6] - a[6]*b[5] - a[5]*b[4] - a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[7]*b[5] - a[6]*b[6] + a[5]*b[2] + a[4]*b[1] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[7]*b[4] + a[6]*b[2] + a[5]*b[6] - a[4]*b[0] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[7]*b[3] - a[6]*b[1] - a[5]*b[0] - a[4]*b[6] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[7]*b[2] + a[6]*b[4] - a[5]*b[5] + a[3]*b[6] - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[7]*b[1] - a[6]*b[3] + a[4]*b[5] - a[3]*b[0] - a[2]*b[6] + a[1]*b[2] + a[0]*b[4],
               a[7]*b[0] + a[5]*b[3] - a[4]*b[4] - a[3]*b[1] + a[2]*b[2] + a[1]*b[6] + a[0]*b[5],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[3]*b[3] - a[2]*b[4] + a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Inner<MV3_1_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[6] - a[6]*b[5] - a[5]*b[4] - a[4]*b[3] + a[3]*b[2] + a[2]*b[1] + a[1]*b[0],
             - a[7]*b[5] - a[6]*b[6] + a[5]*b[2] + a[4]*b[1] - a[3]*b[4] - a[2]*b[3] + a[0]*b[0],
               a[7]*b[4] + a[6]*b[2] + a[5]*b[6] - a[4]*b[0] - a[3]*b[5] + a[1]*b[3] + a[0]*b[1],
             - a[7]*b[3] - a[6]*b[1] - a[5]*b[0] - a[4]*b[6] + a[2]*b[5] + a[1]*b[4] + a[0]*b[2],
               a[7]*b[2] + a[3]*b[6] + a[0]*b[3],
             - a[7]*b[1] - a[2]*b[6] + a[0]*b[4],
               a[7]*b[0] + a[1]*b[6] + a[0]*b[5],
               a[0]*b[6]]
)
    }
}

impl<T> Outer<MV3_1_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_1_2_3<T>;
    fn outer(self, other: MV3_1_2_3<T>) -> MV3_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_1_2_3(
            [  a[0]*b[0],
               a[0]*b[1],
               a[0]*b[2],
             - a[2]*b[0] + a[1]*b[1] + a[0]*b[3],
             - a[3]*b[0] + a[1]*b[2] + a[0]*b[4],
             - a[3]*b[1] + a[2]*b[2] + a[0]*b[5],
               a[6]*b[0] - a[5]*b[1] + a[4]*b[2] + a[3]*b[3] - a[2]*b[4] + a[1]*b[5] + a[0]*b[6]]
)
    }
}

impl<T> Mul<MV3_0_1_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn mul(self,  other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[7] - a[6]*b[6] - a[5]*b[5] - a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[7]*b[6] - a[6]*b[7] + a[5]*b[3] + a[4]*b[2] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[7]*b[5] + a[6]*b[3] + a[5]*b[7] - a[4]*b[1] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[7]*b[4] - a[6]*b[2] - a[5]*b[1] - a[4]*b[7] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[7]*b[3] + a[6]*b[5] - a[5]*b[6] + a[4]*b[0] + a[3]*b[7] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
             - a[7]*b[2] - a[6]*b[4] + a[5]*b[0] + a[4]*b[6] - a[3]*b[1] - a[2]*b[7] + a[1]*b[3] + a[0]*b[5],
               a[7]*b[1] + a[6]*b[0] + a[5]*b[4] - a[4]*b[5] - a[3]*b[2] + a[2]*b[3] + a[1]*b[7] + a[0]*b[6],
               a[7]*b[0] + a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6] + a[0]*b[7]]
)
    }
}

impl<T> Inner<MV3_0_1_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn inner(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [- a[7]*b[7] - a[6]*b[6] - a[5]*b[5] - a[4]*b[4] + a[3]*b[3] + a[2]*b[2] + a[1]*b[1] + a[0]*b[0],
             - a[7]*b[6] - a[6]*b[7] + a[5]*b[3] + a[4]*b[2] - a[3]*b[5] - a[2]*b[4] + a[1]*b[0] + a[0]*b[1],
               a[7]*b[5] + a[6]*b[3] + a[5]*b[7] - a[4]*b[1] - a[3]*b[6] + a[2]*b[0] + a[1]*b[4] + a[0]*b[2],
             - a[7]*b[4] - a[6]*b[2] - a[5]*b[1] - a[4]*b[7] + a[3]*b[0] + a[2]*b[6] + a[1]*b[5] + a[0]*b[3],
               a[7]*b[3] + a[4]*b[0] + a[3]*b[7] + a[0]*b[4],
             - a[7]*b[2] + a[5]*b[0] - a[2]*b[7] + a[0]*b[5],
               a[7]*b[1] + a[6]*b[0] + a[1]*b[7] + a[0]*b[6],
               a[7]*b[0] + a[0]*b[7]]
)
    }
}

impl<T> Outer<MV3_0_1_2_3<T>> for MV3_0_1_2_3<T> where T: Num+Neg<Output=T>+Copy {
    type Output = MV3_0_1_2_3<T>;
    fn outer(self, other: MV3_0_1_2_3<T>) -> MV3_0_1_2_3<T> {
        let a = self.0;
        let b = other.0;
        MV3_0_1_2_3(
            [  a[0]*b[0],
               a[1]*b[0] + a[0]*b[1],
               a[2]*b[0] + a[0]*b[2],
               a[3]*b[0] + a[0]*b[3],
               a[4]*b[0] - a[2]*b[1] + a[1]*b[2] + a[0]*b[4],
               a[5]*b[0] - a[3]*b[1] + a[1]*b[3] + a[0]*b[5],
               a[6]*b[0] - a[3]*b[2] + a[2]*b[3] + a[0]*b[6],
               a[7]*b[0] + a[6]*b[1] - a[5]*b[2] + a[4]*b[3] + a[3]*b[4] - a[2]*b[5] + a[1]*b[6] + a[0]*b[7]]
)
    }
}

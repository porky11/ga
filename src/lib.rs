extern crate num_traits;
extern crate vector_space;

pub trait Inner<RHS = Self> {
    type Output;
    fn inner(self, rhs: RHS) -> Self::Output;
}

pub trait Outer<RHS = Self> {
    type Output;
    fn outer(self, rhs: RHS) -> Self::Output;
}

mod auto;

//FIXME: Should every type be exported for every dimension?
pub use auto::{
    //MV0 as Zero0,
    //MV0_0 as Scalar0,
    //MV0 as Vector0,
    //MV0 as Bivector0,
    //MV0_0 as Pseudoscalar0,
    //MV0_0 as Rotor0,
    //MV0_0 as Multivector0,

    //MV1 as Zero1,
    MV1_0 as Scalar1,
    MV1_1 as Vector1,
    //MV1 as Bivector1,
    MV1_1 as Pseudoscalar1,
    //MV1_0 as Rotor1,
    //MV1_0_1 as Multivector1,

    //MV2 as Zero2,
    MV2_0 as Scalar2,
    MV2_1 as Vector2,
    MV2_2 as Bivector2,
    MV2_2 as Pseudoscalar2,
    MV2_0_2 as Rotor2,
    //MV2_0_1_2 as Multivector2,

    //MV3 as Zero3,
    MV3_0 as Scalar3,
    MV3_1 as Vector3,
    MV3_2 as Bivector3,
    MV3_3 as Pseudoscalar3,
    MV3_0_2 as Rotor3,
    //MV3_0_1_2 as Multivector3,
};


mod tests {

    #[test]
    fn get_normal() {
        use Vector2;
        use Pseudoscalar2;
        let v1 = Vector2::<f32>::e1();
        let v2 = Vector2::<f32>::e2();
        let normal = v1*Pseudoscalar2::unit();
        assert_eq!(v2,normal);
    }

    #[test]
    fn cross_product() {
        use Outer;
        use Vector3;
        use Pseudoscalar3;
        let v1 = Vector3::<f32>::e1();
        let v2 = Vector3::<f32>::e2();
        let v3 = Vector3::<f32>::e3();
        let v12 = -v1.outer(v2);
        let normal = v12*Pseudoscalar3::unit();
        assert_eq!(v3,normal);
    }

    #[test]
    fn rotate_vector_2D() {
        use std;
        use Outer;
        use Vector2 as Vector;
        let v1 = Vector::<f32>::e1();
        let v2 = Vector::<f32>::e2();
        let bivector = v1.outer(v2)*std::f32::consts::PI; //rotate 1/4
        let rotor = bivector.exp();
        let v3 = rotor.rotate(v1);
        let arr = v3.vector();
        let (x,y) = (arr[0],arr[1]);
        assert!(x==1.0);
        assert!(y<0.0001);
    }
    
    //TODO: redefine rotate to return the correct output type
    #[test]
    fn rotate_vector_3D() {
        use std;
        use Outer;
        use Vector3 as Vector;
        let v1 = Vector::<f32>::e1();
        let v2 = Vector::<f32>::e2();
        let bivector = v1.outer(v2)*std::f32::consts::PI; //rotate 1/4
        let rotor = bivector.exp();
        let v3 = rotor.rotate(v1);
        let arr = v3.vector();
        let (x,y,z) = (arr[0],arr[1],arr[2]);
        assert!(x==1.0);
        assert!(y<0.0001);
        assert!(z==0.0);
    }

    #[test]
    fn complex_numbers() {
        use Outer;
        use vector_space::InnerSpace;
        use Rotor2 as Complex;
        use Bivector2 as Imag;
        use Vector2 as consts;

        let i: Imag<i32> = consts::e1().outer(consts::e2());
        let x: i32 = (i*i).value();
        assert_eq!(x,-1);

        let i: Complex<i8> = consts::e1()*consts::e2();
        let l: Complex<i8> = 1.into();
        let x = ((l+i)*(l-i)).magnitude2();
        assert_eq!(x,4);
    }
}

